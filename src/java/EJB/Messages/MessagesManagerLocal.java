/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.Messages;

import EJB.Exceptions.Error400Exception;
import JPA.EMessage;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Dav__
 */
@Local
public interface MessagesManagerLocal {

    EMessage findMessage(Long messageId) throws Error400Exception;

    boolean sendMessage(String message, Long senderId, String receiverUsername) throws Error400Exception;

    List<EMessage> getAllSentMessages(Long userId, Integer offset) throws Error400Exception;
    
    List<EMessage> getAllReceivedMessages(Long userId, Integer offset) throws Error400Exception;

    boolean removeMessage(Long messageId) throws Error400Exception;
    
}

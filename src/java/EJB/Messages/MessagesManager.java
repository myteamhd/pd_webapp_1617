/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.Messages;

import EJB.Exceptions.Error400Exception;
import EJB.account.AccountManagerLocal;
import EJB.utils.DAOLocal;
import JPA.EMessage;
import JPA.EUser;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Dav__
 */
@Singleton
public class MessagesManager implements MessagesManagerLocal {
    
    @EJB
    DAOLocal dao;
    
    @EJB
    AccountManagerLocal accountManager;

    @Override
    public EMessage findMessage(Long messageId) throws Error400Exception{
        
        if(messageId == null)
            throw new Error400Exception("Inválid message id!");
        
        Query q = dao.getEntityManager().createNamedQuery(EMessage.queryFindAllById);
        q.setParameter("id", messageId);
        
        try {
            return (EMessage) q.getSingleResult();
        } catch(NoResultException ex){
            return null;
        }
    }

    @Override
    public boolean sendMessage(String message, Long senderId, String receiverUsername) throws Error400Exception{
        
        if(message == null || senderId == null || receiverUsername == null)
            throw new Error400Exception("Dados inválidos!");
        
        EUser sender = accountManager.getUserById(senderId);
        EUser receiver = accountManager.getUserByUsername(receiverUsername);
        
        EMessage m = new EMessage(message, sender, receiver);
        dao.getEntityManager().persist(m);
        
        return true;
    }

    @Override
    public List<EMessage> getAllSentMessages(Long userId, Integer offset) throws Error400Exception {
        
        if(userId == null)
            throw new Error400Exception("Dados inválidos!");
        
        if(offset == null || offset < 0)
            offset = 0;
        
        EUser user = accountManager.getUserById(userId);
        
        Query q = dao.getEntityManager().createNamedQuery(EMessage.queryFindAllByUserSender);
        q.setParameter("senderId", user);
        q.setFirstResult(offset);
        q.setMaxResults(10);
        
        try {
            return q.getResultList();
        } catch(NoResultException ex){
            return null;
        }
    }
    
    @Override
    public List<EMessage> getAllReceivedMessages(Long userId, Integer offset) throws Error400Exception{
        if(userId == null)
            throw new Error400Exception("Dados inválidos!");
        
        if(offset == null || offset < 0)
            offset = 0;
        
        EUser user = accountManager.getUserById(userId);
        
        Query q = dao.getEntityManager().createNamedQuery(EMessage.queryFindAllByUserReceiver);
        q.setParameter("receiverId", user);
        q.setFirstResult(offset);
        q.setMaxResults(10);
        
        try {
            return q.getResultList();
        } catch(NoResultException ex){
            return null;
        }
    }

    @Override
    public boolean removeMessage(Long messageId) throws Error400Exception {
        Query q = dao.getEntityManager().createNamedQuery(EMessage.queryFindAllById);
        q.setParameter("id", messageId);
        
        EMessage message = (EMessage) q.getSingleResult();
        
        dao.getEntityManager().remove(message);
        
        return true;
    }

    
    
    
    
}

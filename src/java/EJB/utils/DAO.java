/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.utils;

import java.util.HashMap;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Dav__
 */
@Singleton
public class DAO implements DAOLocal {
    
    @PersistenceContext(unitName = "TP_PD1617_WEBAPPPU")
    private EntityManager EM;

    @Override
    public EntityManager getEntityManager() {
        return EM;
    }
    
//    @Override
//    public void persistEntity(Object entityToPersist) throws Exception{
//        //EM.getTransaction().begin();
//        try{
//            EM.persist(entityToPersist);
//            //EM.getTransaction().commit();
//        }catch (Exception e){
//            //EM.getTransaction().rollback();
//            throw e;
//        }
//    }
//    
//    @Override
//    public <T> T mergeEntity(T entity) {
//        //EM.getTransaction().begin();
//        try{
//            T ent = EM.merge(entity);
//            //EM.getTransaction().commit();
//            return ent;
//        }catch (Exception e){
//            //EM.getTransaction().rollback();
//            throw e;
//        }
//    }
//
//    @Override
//    public void removeEntity(Object entity) throws Exception {
//        //EM.getTransaction().begin();
//        try{
//            EM.remove(entity);
//            //EM.getTransaction().commit();
//        }catch (Exception e){
//            //EM.getTransaction().rollback();
//            throw e;
//        }
//    }
//
//    @Override
//    public <T> T findEntity(Class<T> entityClass, Object primaryKey) throws Exception {
//        //EM.getTransaction().begin();
//        try{
//            T ent = EM.find(entityClass, primaryKey);
//            //EM.getTransaction().commit();
//            return ent;
//        }catch (Exception e){
//            //EM.getTransaction().rollback();
//            throw e;
//        }
//    }
//    
//    
//
//    @Override
//    public List namedQuery(String namedQuery, HashMap<String,Object> parameters) throws Exception {
//        
//        //EM.getTransaction().begin();
//        
//        try{
//            Query q = EM.createNamedQuery(namedQuery);
//            
//            if(parameters != null && parameters.size() > 0){
//                for(HashMap.Entry<String, Object> entry : parameters.entrySet()){
//                    q.setParameter(entry.getKey(), entry.getValue());
//                }
//            }
//            
//            List list = q.getResultList();
//            
//            //EM.getTransaction().commit();
//            
//            return list;
//            
//        }catch (Exception e){
//            //EM.getTransaction().rollback();
//            throw e;
//        }
//        
//    }
//
//    @Override
//    public List query(String query) throws Exception {
//        //EM.getTransaction().begin();
//        
//        try{
//            Query q = EM.createQuery(query);
//            
//            List list = q.getResultList();
//            
//            //EM.getTransaction().commit();
//            
//            return list;
//        }catch (Exception e){
//            //EM.getTransaction().rollback();
//            throw e;
//        }
//    }
    
      
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.utils;

import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import javax.persistence.EntityManager;

/**
 *
 * @author Dav__
 */
@Local
public interface DAOLocal {

//    void persistEntity(Object entityToPersist) throws Exception;
//    
//    <T> T mergeEntity(T entity)  throws Exception;
//    
//    void removeEntity(Object entity) throws Exception;
//    
//    <T> T findEntity(Class<T> entityClass, Object primaryKey) throws Exception;
//
//    List namedQuery(String namedQuery, HashMap<String,Object> parameters) throws Exception;
//    
//    List query(String query) throws Exception;

    EntityManager getEntityManager();

    
}

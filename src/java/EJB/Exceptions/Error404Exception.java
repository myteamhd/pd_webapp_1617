/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.Exceptions;

/**
 *
 * @author Dav__
 */
public class Error404Exception extends Exception{

    public Error404Exception(String message) {
        super(message);
    }
    
}

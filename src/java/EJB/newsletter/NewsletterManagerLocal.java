/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.newsletter;

import JPA.ENewsletter;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Dav__
 */
@Local
public interface NewsletterManagerLocal {

    boolean insertNewMessage(String message);

    //String getMessages(int offset);

    List<ENewsletter> getLastNewsletterItems(Integer offset);
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.newsletter;

import JPA.ENewsletter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import EJB.utils.DAOLocal;
import javax.persistence.Query;

/**
 *
 * @author Dav__
 */
@Singleton
public class NewsletterManager implements NewsletterManagerLocal {

    @EJB
    private DAOLocal dao;
    
    @PostConstruct
    private void postConstruct(){
        Query q = dao.getEntityManager().createNamedQuery(ENewsletter.queryFindLastItems);
        List<ENewsletter> list = q.getResultList();
        
        if(list.isEmpty()){
            insertNewMessage("Base de dados inicializada!");
        }
        
    }
    
    @PreDestroy
    private void preDestroy(){
    }
    
    @Override
    public boolean insertNewMessage(String message) {
        
        ENewsletter newsletter = new ENewsletter(message);
        
        try {
            dao.getEntityManager().persist(newsletter);
        } catch (Exception ex) {
            Logger.getLogger(NewsletterManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        return true;
    }
    
    @Override
    public List<ENewsletter> getLastNewsletterItems(Integer offset) {
        
        if(offset == null || offset < 0){
            offset = 0;
        }
        
        try {
            Query q = dao.getEntityManager().createNamedQuery(ENewsletter.queryFindLastItems);
            q.setMaxResults(10);
            q.setFirstResult(offset);
            return q.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(NewsletterManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    } 
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.account;

import EJB.Exceptions.Error400Exception;
import EJB.Exceptions.Error401Exception;
import EJB.Exceptions.Error403Exception;
import EJB.Exceptions.Error500Exception;
import JPA.EUser;
import java.util.List;
import javax.ejb.Local;
/**
 *
 * @author Dav__
 */
@Local
public interface AccountManagerLocal {

    EUser login(String username, String string) throws Error403Exception,Error400Exception ;

    boolean createUser(String name, String username, String password, String address) throws Error400Exception;

    EUser updateUser(EUser user) throws Error400Exception;

    boolean removeUser(Long userId, String username) throws Error400Exception;

    List<EUser> getAllUsers(Integer offset);

//    boolean blockUser(Long id, String username) throws Error400Exception;

    boolean createUserByRequest(String name, String username, String password, String address) throws Error400Exception,Error500Exception;

    boolean logout(EUser user) throws Error400Exception;

    boolean createReativationRequest(String username) throws Error400Exception;

    EUser getUserById(Long userId);

    boolean changePassword(Long userId, String oldPassword, String newPassword, String confPassword) throws Error400Exception, Error401Exception;

    List<EUser> searchUser(String username) throws Error400Exception;

    EUser getUserByUsername(String username);
    
    boolean blockAccount(Long id) throws Error400Exception;

    boolean reactivateAccount(Long id) throws Error400Exception;
    
}

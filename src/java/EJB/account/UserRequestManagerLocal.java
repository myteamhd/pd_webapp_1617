/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.account;

import EJB.Exceptions.Error400Exception;
import JPA.EUser;
import JPA.EUserRequest;
import JPA.RequestTypeEnum;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Dav__
 */
@Local
public interface UserRequestManagerLocal {

    boolean createRequest(EUser user, String message, RequestTypeEnum type);

    boolean closeRequest(Long id);

    List<EUserRequest> getAllActive(Integer offset);

    EUserRequest findRequest(Long requestId) throws Error400Exception;
    
    boolean acceptRequest(Long requestId) throws Error400Exception;
    
    boolean rejectRequest(Long requestId) throws Error400Exception;
    
}

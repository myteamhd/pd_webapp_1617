/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.account;

import EJB.Exceptions.Error400Exception;
import EJB.Exceptions.Error401Exception;
import EJB.Exceptions.Error403Exception;
import EJB.Exceptions.Error500Exception;
import EJB.account.AccountManagerLocal;
import JPA.EUser;
import JPA.UserStatusEnum;
import JPA.UserTypeEnum;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import EJB.newsletter.NewsletterManagerLocal;
import EJB.utils.DAOLocal;
import JPA.EUserRequest;
import JPA.RequestTypeEnum;
import java.util.List;
import java.util.logging.Level;
import javax.faces.application.FacesMessage;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Dav__
 */
@Singleton
public class AccountManager implements AccountManagerLocal {

    @EJB
    private DAOLocal dao;
    
    @EJB
    private SessionManagerLocal sessionManager;
    
    @EJB
    private UserRequestManagerLocal requestManager;
    
    @EJB
    private NewsletterManagerLocal newsletterManager;
    
    @PostConstruct
    private void postConstruct(){

        Query q = dao.getEntityManager().createNamedQuery(EUser.queryFindAll);
        List<EUser> list = q.getResultList();
        
        if(list.isEmpty()){
            EUser admin = new EUser("Administrator", "admin", "admin", UserStatusEnum.ACTIVE, UserTypeEnum.ADMIN, " ", (float) 100.0);
            dao.getEntityManager().persist(admin);
            EUser user = new EUser("Hugo David", "hugo", "mercurio26", UserStatusEnum.ACTIVE, UserTypeEnum.NORMAL, " ", (float) 100.0);
            dao.getEntityManager().persist(user);
        }

    }
    
    @PreDestroy
    private void preDestroy(){

    }
    
    
    @Override
    public EUser login(String username, String password) throws Error403Exception,Error400Exception{

        Query q = dao.getEntityManager().createNamedQuery(EUser.queryFindByUsernamePasswordStatus);
        q.setParameter("username", username);
        q.setParameter("password", password);
        q.setParameter("status", UserStatusEnum.ACTIVE);
        
        List<EUser> users = q.getResultList();
        
        if(users != null && users.size() > 0){
            
            EUser user = (EUser) users.get(0);

            if(sessionManager.hasActiveSession(user) == null){
                
                sessionManager.createSession(user);
                
                return user;
            }else{
                throw new Error403Exception("Utilizador já está logado!");
            }
            
        }else{
            throw new Error400Exception("Utilizador não encontrado ou credenciais inválidas!");
        }
        
    }
    
    @Override
    public boolean logout(EUser user) throws Error400Exception{
        return sessionManager.finishSession(user);
    }
    

    @Override
    public boolean createUser(String name, String username, String password, String address) throws Error400Exception{
            
        Query q = dao.getEntityManager().createNamedQuery(EUser.queryFindByUsername);
        q.setParameter("username", username);

        List<EUser> users = q.getResultList();

        if(users.isEmpty()){
            EUser user = new EUser(name, username, password, address);
            dao.getEntityManager().persist(user);
            return true;
        }else{
            throw new Error400Exception("Utilizador já existe!");
        }

    }
    
    @Override
    public boolean createUserByRequest(String name, String username, String password, String address) throws Error400Exception,Error500Exception{
        
        if(this.createUser(name, username, password, address)){
            Query q = dao.getEntityManager().createNamedQuery(EUser.queryFindByUsername);
            q.setParameter("username", username);
            
            List<EUser> users = q.getResultList();
            
            if(users.size() > 0){
                EUser user = users.get(0);
                
                requestManager.createRequest(user, "Utilizador " + user.getUsername() + " fez um pedido de registo!", RequestTypeEnum.ACCESS);
                
                return true;
            }else{
                throw new Error500Exception("Alguma coisa correu mal!");
            }
           
        }
        return false;
    }
    
    @Override
    public boolean createReativationRequest(String username) throws Error400Exception{
        
        Query q = dao.getEntityManager().createNamedQuery(EUser.queryFindByUsernameStatus);
        q.setParameter("username", username);
        q.setParameter("status", UserStatusEnum.BLOCKED);

        List<EUser> users = q.getResultList();

        if(!users.isEmpty()){
            EUser user = users.get(0);

            requestManager.createRequest(user, "Utilizador " + user.getUsername() + " fez um pedido de reativação de conta!", RequestTypeEnum.REATIVATION);

            return true;
        }else{
            throw new Error400Exception("Não foi encontrado nenhum utilizador!");
        }
        
    }

    @Override
    public EUser updateUser(EUser user) throws Error400Exception {
        return dao.getEntityManager().merge(user);
    }
    
    

    @Override
    public boolean removeUser(Long userId, String username) throws Error400Exception {
        
        if(username != null && userId != null){

            Query q = dao.getEntityManager().createNamedQuery(EUser.queryFindByIdUsername);
            q.setParameter("id", userId);
            q.setParameter("username", username);
            
            List<EUser> users = q.getResultList();

            if(!users.isEmpty()){
                EUser u = (EUser) users.get(0);
                
                dao.getEntityManager().remove(u);

                return true;
            }else{
                throw new Error400Exception("Utilizador não encontrado!");
            }
            
        }else{
            throw new Error400Exception("Dados inválidos!");
        }
    }

    @Override
    public List<EUser> getAllUsers(Integer offset) {
        
        if(offset == null)
            offset = 0;
       
        Query q = dao.getEntityManager().createNamedQuery(EUser.queryFindAll);
        q.setFirstResult(offset);
        q.setMaxResults(10);
        
        try {
            return q.getResultList();
        } catch(NoResultException ex){
            return null;
        }

    }

    @Override
    public EUser getUserById(Long userId) {
        
        Query q = dao.getEntityManager().createNamedQuery(EUser.queryFindById);
        q.setParameter("id", userId);
        
        try {
            return (EUser) q.getSingleResult();
        } catch(NoResultException ex){
            return null;
        }

    }
    
    @Override
    public EUser getUserByUsername(String username) {
        Query q = dao.getEntityManager().createNamedQuery(EUser.queryFindByUsername);
        q.setParameter("username", username);
        
        try {
            return (EUser) q.getSingleResult();
        } catch(NoResultException ex){
            return null;
        }
    }

    @Override
    public boolean changePassword(Long userId, String oldPassword, String newPassword, String confPassword) throws Error400Exception, Error401Exception{
        
        if(newPassword.equals(confPassword)){
             Query q = dao.getEntityManager().createNamedQuery(EUser.queryFindById);
             q.setParameter("id", userId);
             
             EUser user = (EUser) q.getSingleResult();
             
             if(user.getPassword().equals(oldPassword)){
                 user.setPassword(confPassword);
                 dao.getEntityManager().merge(user);
             }else{
                 throw new Error401Exception("Password errada!");
             }
        }else{
            throw new Error400Exception("Passwords não coincidem!");
        }

        return true;
    }

    @Override
    public List<EUser> searchUser(String username) throws Error400Exception{
        
        if(username == null){
            throw new Error400Exception("Dados inválidos!");
        }
        
        Query q = dao.getEntityManager().createNamedQuery(EUser.querySearchByUsername);
        q.setParameter("username", "%" + username + "%");
        q.setParameter("status", UserStatusEnum.ACTIVE);
        
        try {
            return q.getResultList();
        } catch(NoResultException ex){
            return null;
        }
    }

    @Override
    public boolean blockAccount(Long id) throws Error400Exception {

        EUser user = getUserById(id);
        
        user.setStatus(UserStatusEnum.BLOCKED);
        
        dao.getEntityManager().merge(user);
        
        newsletterManager.insertNewMessage("Utilizador " + user.getName() + " foi bloqueado pelo adiministrador!");
        
        return true;

    }

    @Override
    public boolean reactivateAccount(Long id) throws Error400Exception {
        EUser user = getUserById(id);
        
        user.setStatus(UserStatusEnum.ACTIVE);
        
        dao.getEntityManager().merge(user);
        
        newsletterManager.insertNewMessage("Utilizador " + user.getName() + " foi reativado pelo adiministrador!");
        
        return true;
    }

   
    
}

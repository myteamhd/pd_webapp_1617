/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.account;


import EJB.Exceptions.Error400Exception;
import JPA.EUser;
import JPA.EUserSession;
import javax.ejb.Local;

/**
 *
 * @author Dav__
 */
@Local
public interface SessionManagerLocal {

    EUserSession hasActiveSession(EUser user);

    boolean createSession(EUser user);

    boolean finishSession(EUser user) throws Error400Exception;
    
}

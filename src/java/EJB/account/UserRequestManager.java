/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.account;

import EJB.Exceptions.Error400Exception;
import EJB.newsletter.NewsletterManagerLocal;
import EJB.utils.DAOLocal;
import JPA.EUser;
import JPA.EUserRequest;
import JPA.RequestTypeEnum;
import JPA.UserStatusEnum;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Dav__
 */
@Singleton
public class UserRequestManager implements UserRequestManagerLocal{

    @EJB
    private DAOLocal dao;
    
    @EJB
    private AccountManagerLocal accountManager;
    
    @EJB
    private NewsletterManagerLocal newsletterManager;
    
    @Override
    public boolean createRequest(EUser user, String message, RequestTypeEnum type){
        EUserRequest request = new EUserRequest(message, type, Boolean.FALSE, user);
                
        dao.getEntityManager().persist(request);
        
        return true;
    }

    @Override
    public boolean closeRequest(Long id){
        
        Query q = dao.getEntityManager().createNamedQuery(EUserRequest.queryFindActiveRequestByIdClosed);
        q.setParameter("id", id);
        q.setParameter("closed", false);
        
        List<EUserRequest> requests = q.getResultList();
        
        if(!requests.isEmpty()){
            EUserRequest r = requests.get(0);
            r.setClosed(true);
            dao.getEntityManager().merge(r);
            return true;
        }
        
        return false;
    }
    
    @Override
    public boolean acceptRequest(Long requestId) throws Error400Exception{
        
        EUserRequest request = findRequest(requestId);
        EUser user;
        
        switch(request.getType()){
            case ACCESS:
                user = request.getUserId();
                user.setStatus(UserStatusEnum.ACTIVE);
                accountManager.updateUser(user);
                newsletterManager.insertNewMessage("Utilizador " + user.getName() + " foi aceite!");
                break;
                
            case REATIVATION:
                user = request.getUserId();
                user.setStatus(UserStatusEnum.ACTIVE);
                accountManager.updateUser(user);
                newsletterManager.insertNewMessage("Pedido de reativação do utilziador " + user.getName() + " foi aceite!");
                break;
                
            default:
                break;
        }
        
        request.setClosed(true);
        dao.getEntityManager().merge(request);
        
        return true;
    }
    
    @Override
    public boolean rejectRequest(Long requestId) throws Error400Exception{
        
        EUserRequest request = findRequest(requestId);
        EUser user;
        
        switch(request.getType()){
            case ACCESS:
                user = request.getUserId();
                accountManager.removeUser(user.getId(), user.getUsername());
                newsletterManager.insertNewMessage("Utilizador " + user.getName() + " foi rejeitado!");
                break;
                
            case REATIVATION:
                user = request.getUserId();
                newsletterManager.insertNewMessage("Pedido de reativação do utilziador " + user.getName() + " foi rejeitado!");
                break;
                
            default:
                break;
        }
        
//        request.setClosed(true);
//        dao.getEntityManager().merge(request);
        
        return true;
    }

    @Override
    public List<EUserRequest> getAllActive(Integer offset) {
        
        if(offset == null)
            offset = 0;
       
        Query q = dao.getEntityManager().createNamedQuery(EUserRequest.queryFindAllActive);
        q.setParameter("closed", false);
        q.setFirstResult(offset);
        q.setMaxResults(10);
        
        try {
            return q.getResultList();
        } catch(NoResultException ex){
            return null;
        }
    }

    @Override
    public EUserRequest findRequest(Long requestId) throws Error400Exception{
        
        if(requestId == null){
            throw new Error400Exception("ID inválido!");
        }
        
        try {
            return dao.getEntityManager().find(EUserRequest.class, requestId);
        } catch(NoResultException ex){
            return null;
        }
    }

    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.account;

import EJB.Exceptions.Error400Exception;
import EJB.Exceptions.Error403Exception;
import EJB.utils.DAOLocal;
import JPA.EUser;
import JPA.EUserSession;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.Query;

/**
 *
 * @author Dav__
 */
@Singleton
public class SessionManager implements SessionManagerLocal{

    @EJB
    private DAOLocal dao;
    
    @PostConstruct
    public void removeActiveSessions(){
        Query q = dao.getEntityManager().createNamedQuery(EUserSession.queryfindActiveSessions);
        q.setParameter("active", true);
        
        List<EUserSession> sessions = q.getResultList();
        
        for(EUserSession session : sessions){
            session.setActive(false);
            dao.getEntityManager().merge(session);
        }
    }
    
    @Override
    public EUserSession hasActiveSession(EUser user){
        
        Query q = dao.getEntityManager().createNamedQuery(EUserSession.queryfindSessionByID);
        q.setParameter("user", user);
        q.setParameter("active", true);
            
        List<EUserSession> sessions = q.getResultList();
        
        if(!sessions.isEmpty()){
            return sessions.get(0);
        }else{
            return null;
        }
    }

    @Override
    public boolean createSession(EUser user) {
        
        EUserSession session = new EUserSession(new Date(Calendar.getInstance().getTimeInMillis()), true, user);
        dao.getEntityManager().persist(session);
        
        return true;
    }

    @Override
    public boolean finishSession(EUser user) throws Error400Exception{
        
        EUserSession session = this.hasActiveSession(user);
        
        if(session != null){
            session.setActive(false);
            dao.getEntityManager().merge(session);
            return true;
        }else{
            throw new Error400Exception("Sessão inválida!");
        }
        
    }

    
   
}

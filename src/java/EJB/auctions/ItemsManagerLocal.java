/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.auctions;

import EJB.Exceptions.Error400Exception;
import JPA.EItem;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Dav__
 */
@Local
public interface ItemsManagerLocal {

    boolean createItem(String name, String description, Float initial_price, Float buy_price, Date limit_date, Long categorieId, Long userId) throws Error400Exception;

    EItem updateItem(EItem item) throws Error400Exception;

    boolean removeItem(Long id) throws Error400Exception;

    EItem findItembyId(Long id) throws Error400Exception;
    
    EItem findItemByIdByUser(Long id, Long userId) throws Error400Exception;

    List<EItem> getAllItems(Integer offset) throws Error400Exception;
    
    List<EItem> getAllItemsByUser(Long userId, Integer offset) throws Error400Exception;
    
    List<EItem> getAllItemsInAuction(Integer offset) throws Error400Exception;

    boolean cancelOwnItem(Long id) throws Error400Exception;
    
    boolean cancelItem(Long id) throws Error400Exception;
    
    boolean reactivateItem(Long id) throws Error400Exception;
    
}

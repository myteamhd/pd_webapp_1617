/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.auctions;

import EJB.account.AccountManagerLocal;
import EJB.utils.DAOLocal;
import JPA.EItem;
import JPA.ETransaction;
import JPA.EUser;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Dav__
 */
@Singleton
public class TransactionManager implements TransactionManagerLocal {

    @EJB
    DAOLocal dao;
    
    @EJB
    AccountManagerLocal accountManager;
    
    @Override
    public boolean createTransaction(Float value, EUser user, EItem item) {
        
        ETransaction transaction = new ETransaction(value, user, item);
        
        dao.getEntityManager().persist(transaction);
        
        return true;
        
    }

    @Override
    public List<ETransaction> getAllTransactionsByUser(Long userId, Integer offset) {
       
        if(offset == null || offset < 0){
            offset = 0;
        }
        
        EUser user = accountManager.getUserById(userId);
        
        Query q = dao.getEntityManager().createNamedQuery(ETransaction.queryFindAllByUserId);
        q.setParameter("userId", user);
        q.setMaxResults(10);
        q.setFirstResult(offset);
        
        try {
            return q.getResultList();
        } catch(NoResultException ex){
            return null;
        }
        
    }

    @Override
    public List<ETransaction> getLastTransactions() {
     
        Query q = dao.getEntityManager().createNamedQuery(ETransaction.queryFindAll);
        q.setMaxResults(3);
        
        try {
            return q.getResultList();
        } catch(NoResultException ex){
            return null;
        }
    }
  
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.auctions;

import EJB.Exceptions.Error400Exception;
import EJB.account.AccountManagerLocal;
import EJB.utils.DAOLocal;
import JPA.EBid;
import JPA.EItem;
import JPA.EItemCategorie;
import JPA.EUser;
import JPA.ItemStatusEnum;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;

/**
 *
 * @author Dav__
 */
@Singleton
public class AuctionsManager implements AuctionsManagerLocal {

    @EJB
    DAOLocal dao;
    
    @EJB
    AccountManagerLocal accountManager;
    
    @EJB
    ItemsManagerLocal itemsManager;
    
    @EJB
    TransactionManagerLocal transactionManager;
    
    @PostConstruct
    public void postConstruct(){
//        try {
//            createBid((long) 1, (long) 1, (float) 11);
//            createBid((long) 1, (long) 1, (float) 12);
//        } catch (Error400Exception ex) {
//            Logger.getLogger(AuctionsManager.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    @Override
    public boolean createBid(Long userId, Long itemId, Float bid_value) throws Error400Exception {
        
        EUser user = accountManager.getUserById(userId);
        EItem item = itemsManager.findItembyId(itemId);
        
        if(user.getMoney() < bid_value){
            return false;
        }
        
        EBid bid = new EBid(bid_value, user, item);
        
        dao.getEntityManager().persist(bid);
        
        //TODO send notification to followers

        if(bid_value >= item.getPrice_buy()){

            item.setStatus(ItemStatusEnum.SOLD);
            itemsManager.updateItem(item);
            
            //create transaction
            transactionManager.createTransaction(bid_value, user, item);
            
            user.setMoney(user.getMoney() - bid_value);
            accountManager.updateUser(user);
        }
        
        return true;
        
    }
    
    
}

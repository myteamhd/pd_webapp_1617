/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.auctions;

import EJB.Exceptions.Error400Exception;
import EJB.Exceptions.Error404Exception;
import EJB.utils.DAOLocal;
import JPA.EItemCategorie;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Dav__
 */
@Singleton
public class ItemCategoriesManager implements ItemCategoriesManagerLocal {

    @EJB
    DAOLocal dao;
    
    @PostConstruct
    private void postConstruct(){
        
        Query q = dao.getEntityManager().createNamedQuery(EItemCategorie.queryFindAll);
        List<EItemCategorie> categories = q.getResultList();
        
        if(categories.isEmpty()){
            
            try {
                createItemCategorie("Arte", "Categoria de peças de arte");
                createItemCategorie("Imóveis", "Categoria de imóveis");
            } catch (Error400Exception ex) {
                Logger.getLogger(ItemCategoriesManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

    @Override
    public boolean createItemCategorie(String name, String description) throws Error400Exception{
        
        if(name == null || name.isEmpty()){
            throw new Error400Exception("É necessário inserir um nome para criar uma categoria!");
        }
        
        EItemCategorie categorie = new EItemCategorie(name, description);
        
        dao.getEntityManager().persist(categorie);
        
        return true;
    }

    @Override
    public EItemCategorie updateItemCategorie(EItemCategorie categorie) throws Error400Exception {
        
        if(categorie == null){
            throw new Error400Exception("Categoria inválida!");
        }
        
        return dao.getEntityManager().merge(categorie);
    }

    @Override
    public boolean removeItemCategorie(Long categorieId) throws Error404Exception {
        
        Query q = dao.getEntityManager().createNamedQuery(EItemCategorie.queryFindById);
        q.setParameter("id", categorieId);
        
        EItemCategorie categorie = (EItemCategorie) q.getSingleResult();
        
        if(categorie != null){
            dao.getEntityManager().remove(categorie);
        }else{
            throw new Error404Exception("Categoria não encontrada!");
        }
        return true;
    }

    @Override
    public List<EItemCategorie> getAllItemCategories(Integer offset) {
        
        if(offset == null || offset < 0){
            offset = 0;
        }
        
        Query q = dao.getEntityManager().createNamedQuery(EItemCategorie.queryFindAll);
        q.setMaxResults(10);
        q.setFirstResult(offset);
        
        try{
            return q.getResultList();
        } catch(NoResultException ex){
            return null;
        }
    }

    @Override
    public EItemCategorie findCategorieById(Long id) throws Error400Exception {
        
        if(id==null){
            throw new Error400Exception("ID inválido!");
        }
        
        Query q = dao.getEntityManager().createNamedQuery(EItemCategorie.queryFindById);
        q.setParameter("id", id);
        
        try {
            return (EItemCategorie) q.getSingleResult();
        } catch(NoResultException ex){
            return null;
        }
    }

    @Override
    public List<EItemCategorie> getAllItemCategoriesWithoutOffset() {
        Query q = dao.getEntityManager().createNamedQuery(EItemCategorie.queryFindAll);
        
        try {
            return q.getResultList();
        } catch(NoResultException ex){
            return null;
        }
    }
    
    
    
    
    
}

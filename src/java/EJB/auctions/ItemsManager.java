/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.auctions;

import EJB.Exceptions.Error400Exception;
import EJB.account.AccountManagerLocal;
import EJB.newsletter.NewsletterManagerLocal;
import EJB.utils.DAOLocal;
import JPA.EItem;
import JPA.EItemCategorie;
import JPA.EUser;
import JPA.ItemStatusEnum;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Dav__
 */
@Singleton
public class ItemsManager implements ItemsManagerLocal {

    @EJB
    DAOLocal dao;
    
    @EJB
    ItemCategoriesManagerLocal itemCategoriesManager;
    
    @EJB
    NewsletterManagerLocal newsletterManager;
    
    @EJB
    AccountManagerLocal accountManager;
   
    
    @PostConstruct
    public void postConstruct(){
        Query q = dao.getEntityManager().createNamedQuery(EItem.queryFindAll);
        
        List<EItem> items = q.getResultList();
        
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 1);
        Date d = new Date(cal.getTimeInMillis());
        
        if(items.isEmpty()){
            try {
                createItem("Item Teste", "Item de teste", (float) 10.0, (float) 100.0, d, (long) 1, (long) 1);
            } catch (Error400Exception ex) {
                Logger.getLogger(ItemsManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public boolean createItem(String name, String description, Float initial_price, Float buy_price, Date limit_date, Long categorieId, Long userId) throws Error400Exception {
        
        EUser user = accountManager.getUserById(userId);
        EItemCategorie categorie = itemCategoriesManager.findCategorieById(categorieId);
        
        EItem item = new EItem(name, description, initial_price, buy_price, limit_date, categorie, user);
        
        dao.getEntityManager().persist(item);

        return true;
    }

    @Override
    public EItem updateItem(EItem item) throws Error400Exception {
        if(item == null){
            throw new Error400Exception("Item inválido");
        }
        EItem i = dao.getEntityManager().merge(item);
        
        //newsletterManager.insertNewMessage("Item " + i.getName() + " foi atualizado pelo adiministrador!");
        
        return i;
    }
    
    @Override
    public boolean cancelOwnItem(Long id) throws Error400Exception {
        EItem item = findItembyId(id);
        
        item.setStatus(ItemStatusEnum.CANCELED);
        
        dao.getEntityManager().merge(item);

        return true;
    }

    @Override
    public boolean cancelItem(Long id) throws Error400Exception {
        
        EItem item = findItembyId(id);
        
        item.setStatus(ItemStatusEnum.CANCELED);
        
        dao.getEntityManager().merge(item);
        
        newsletterManager.insertNewMessage("Item " + item.getName() + " foi cancelado pelo adiministrador!");
        
        return true;
    }

    @Override
    public boolean reactivateItem(Long id) throws Error400Exception {
        
        EItem item = findItembyId(id);
        
        item.setStatus(ItemStatusEnum.ONBIDDING);
        
        dao.getEntityManager().merge(item);
        
        newsletterManager.insertNewMessage("Item " + item.getName() + " foi reativado pelo adiministrador!");
        
        return true;
    }
    
    
    @Override
    public boolean removeItem(Long id) throws Error400Exception {
        
        EItem item = findItembyId(id);
        
        dao.getEntityManager().remove(item);
        
        return true;
    }

    @Override
    public EItem findItembyId(Long id) throws Error400Exception {
        
        if(id == null){
            throw new Error400Exception("Id inválido!");
        }
        
        Query q = dao.getEntityManager().createNamedQuery(EItem.queryFindById);
        q.setParameter("id", id);
        
        try {
            return (EItem) q.getSingleResult();
        } catch(NoResultException ex){
            return null;
        }
    }
    
    @Override
    public EItem findItemByIdByUser(Long id, Long userId) throws Error400Exception {
        
        if(id == null || userId == null){
            throw new Error400Exception("Id inválido!");
        }
        
        EUser user = accountManager.getUserById(userId);
        
        Query q = dao.getEntityManager().createNamedQuery(EItem.queryFindAllByIdByUserId);
        q.setParameter("id", id);
        q.setParameter("userId", user);    
        
        try {
            return (EItem) q.getSingleResult();
        } catch(NoResultException ex){
            return null;
        }
        
    }

    @Override
    public List<EItem> getAllItems(Integer offset) throws Error400Exception {
        if(offset == null || offset < 0){
            offset = 0;
        }
        
        Query q = dao.getEntityManager().createNamedQuery(EItem.queryFindAll);
        q.setMaxResults(10);
        q.setFirstResult(offset);
        
        try {
            return q.getResultList();
        } catch(NoResultException ex){
            return null;
        }
    }

    @Override
    public List<EItem> getAllItemsByUser(Long userId, Integer offset) throws Error400Exception {
        if(userId == null){
            throw new Error400Exception("Id inválido!");
        }
        
        if(offset == null || offset < 0){
            offset = 0;
        }
        
        EUser user = accountManager.getUserById(userId);
        
        Query q = dao.getEntityManager().createNamedQuery(EItem.queryFindAllByUserId);
        q.setParameter("userId", user);
        q.setMaxResults(10);
        q.setFirstResult(offset);
        
        try {
            return q.getResultList();
        } catch(NoResultException ex){
            return null;
        }
    }

    @Override
    public List<EItem> getAllItemsInAuction(Integer offset) throws Error400Exception {
        if(offset == null || offset < 0){
            offset = 0;
        }
        
        Query q = dao.getEntityManager().createNamedQuery(EItem.queryFindAllInAuction);
        q.setParameter("status", ItemStatusEnum.ONBIDDING);
        q.setParameter("date", new Date());
        q.setMaxResults(10);
        q.setFirstResult(offset);
        
        try {
            return q.getResultList();
        } catch(NoResultException ex){
            return null;
        }
    }
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.auctions;

import EJB.Exceptions.Error400Exception;
import JPA.EItem;
import JPA.EUser;
import javax.ejb.Local;

/**
 *
 * @author Dav__
 */
@Local
public interface AuctionsManagerLocal {
    
    boolean createBid(Long userId, Long itemId, Float bid_value) throws Error400Exception;
    
}

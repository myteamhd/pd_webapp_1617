/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.auctions;

import EJB.Exceptions.Error400Exception;
import EJB.Exceptions.Error404Exception;
import JPA.EItemCategorie;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Dav__
 */
@Local
public interface ItemCategoriesManagerLocal {

    boolean createItemCategorie(String name, String description) throws Error400Exception;

    EItemCategorie updateItemCategorie(EItemCategorie categorie) throws Error400Exception;

    boolean removeItemCategorie(Long categorieId) throws Error404Exception;

    List<EItemCategorie> getAllItemCategories(Integer offset);

    EItemCategorie findCategorieById(Long id) throws Error400Exception;

    List<EItemCategorie> getAllItemCategoriesWithoutOffset();
    
}

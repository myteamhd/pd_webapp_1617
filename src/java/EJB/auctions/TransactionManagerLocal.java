/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.auctions;

import JPA.EItem;
import JPA.ETransaction;
import JPA.EUser;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Dav__
 */
@Local
public interface TransactionManagerLocal {
    
    boolean createTransaction(Float value, EUser user, EItem item);
    
    List<ETransaction> getAllTransactionsByUser(Long userId, Integer offset);
    
    List<ETransaction> getLastTransactions();
}

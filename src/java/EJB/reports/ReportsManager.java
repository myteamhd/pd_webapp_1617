/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.reports;

import EJB.Exceptions.Error400Exception;
import EJB.account.AccountManagerLocal;
import EJB.auctions.ItemsManagerLocal;
import EJB.utils.DAOLocal;
import JPA.EItem;
import JPA.EReport;
import JPA.EUser;
import JPA.EUserRequest;
import JPA.ReportTypeEnum;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Dav__
 */
@Singleton
public class ReportsManager implements ReportsManagerLocal {

    
    @EJB
    DAOLocal dao;
    
    @EJB
    AccountManagerLocal accountManager;
    
    @EJB
    ItemsManagerLocal itemsManager;
    
    @Override
    public boolean reportUser(String motive, Long reporterId, Long user_reportedId) throws Error400Exception{
        
        EUser reporter = accountManager.getUserById(reporterId);
        EUser user_reported = accountManager.getUserById(user_reportedId);
        
        EReport request = new EReport(motive, ReportTypeEnum.USER, reporter, user_reported);
                
        dao.getEntityManager().persist(request);
        
        return true;
    }

    @Override
    public boolean reportItem(String motive, Long reporterId, Long item_reportedId) throws Error400Exception {
        EUser reporter = accountManager.getUserById(reporterId);
        EItem item_reported = itemsManager.findItembyId(item_reportedId);
        
        EReport request = new EReport(motive, ReportTypeEnum.ITEM, reporter, item_reported);
                
        dao.getEntityManager().persist(request);
        
        return true;
    }
    
    @Override
    public boolean closeReport(Long id){
        
        Query q = dao.getEntityManager().createNamedQuery(EReport.queryFindActiveRequestByIdClosed);
        q.setParameter("id", id);
        q.setParameter("closed", false);
        
        try{
            EReport report = (EReport) q.getSingleResult();
            report.setClosed(true);
            dao.getEntityManager().merge(report);
            return true;
        }catch(NoResultException ex){
            return false;
        }

    }

    @Override
    public List<EReport> getReports(Integer offset) throws Error400Exception {
        if(offset == null)
            offset = 0;
       
        Query q = dao.getEntityManager().createNamedQuery(EReport.queryFindAllActive);
        q.setParameter("closed", false);
        q.setFirstResult(offset);
        q.setMaxResults(10);
        
        try {
            return q.getResultList();
        } catch(NoResultException ex){
            return null;
        }
    }
    
    
    
}

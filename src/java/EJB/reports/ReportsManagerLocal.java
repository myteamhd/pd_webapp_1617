/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB.reports;

import EJB.Exceptions.Error400Exception;
import JPA.EReport;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Dav__
 */
@Local
public interface ReportsManagerLocal {
    
    boolean reportUser(String motive, Long reporterId, Long user_reportedId) throws Error400Exception;
    
    boolean reportItem(String motive, Long reporterId, Long item_reportedId) throws Error400Exception;
    
    boolean closeReport(Long id);
    
    List<EReport> getReports(Integer offset) throws Error400Exception;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Dav__
 */
@Entity
@Table(name="pd_user_session")
@NamedQueries({
    @NamedQuery(name = "EUserSession.findSessionByID", query="SELECT s FROM EUserSession s WHERE s.userId = :user AND s.active = :active"),
    @NamedQuery(name = "EUserSession.findActiveSessions", query="SELECT s FROM EUserSession s WHERE s.active = :active")
})
public class EUserSession implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static final String queryfindSessionByID = "EUserSession.findSessionByID";
    public static final String queryfindActiveSessions = "EUserSession.findActiveSessions";
    
    //*****************************************************
    //*                 Entity variables
    //*****************************************************
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Basic(optional = false)
    @Column(name = "createdAt")
    private Date createdAt;
    
    @Basic(optional = false)
    @Column(name = "active")
    private Boolean active;
    
    @JoinColumn(name = "userId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EUser userId;

    //*****************************************************
    //*             CONSTRUCTORS / GETS / SETS
    //*****************************************************
    public EUserSession() {
    }

    public EUserSession(Long id, Date createdAt, Boolean active, EUser userId) {
        this.id = id;
        this.createdAt = createdAt;
        this.active = active;
        this.userId = userId;
        this.createdAt = new Date();
    }

    public EUserSession(Date createdAt, Boolean active, EUser userId) {
        this.createdAt = createdAt;
        this.active = active;
        this.userId = userId;
        this.createdAt = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public EUser getUserId() {
        return userId;
    }

    public void setUserId(EUser userId) {
        this.userId = userId;
    }

    //*****************************************************
    //*                 Other functions
    //*****************************************************
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EUserSession)) {
            return false;
        }
        EUserSession other = (EUserSession) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JPA.EUserSession[ id=" + id + " ]";
    }
    
}

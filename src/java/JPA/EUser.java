/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dav__
 */
@Entity
@Table(name="pd_user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EUser.findAll", query="SELECT u FROM EUser u"),
    @NamedQuery(name = "EUser.findById", query="SELECT u FROM EUser u WHERE u.id = :id "),
    @NamedQuery(name = "EUser.findByUsername", query="SELECT u FROM EUser u WHERE u.username = :username "),
    @NamedQuery(name = "EUser.findByUsernameStatus", query="SELECT u FROM EUser u WHERE u.username = :username AND u.status = :status "),
    @NamedQuery(name = "EUser.findByIdUsername", query="SELECT u FROM EUser u WHERE u.id = :id AND u.username = :username "),
    @NamedQuery(name = "EUser.findByUsernamePasswordStatus", query="SELECT u FROM EUser u WHERE u.username = :username AND u.password = :password AND u.status = :status"),
    @NamedQuery(name = "EUser.searchByUsername", query = "SELECT u FROM EUser u WHERE u.username LIKE :username AND u.status = :status")
})

public class EUser implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static final String queryFindAll = "EUser.findAll";
    public static final String queryFindById = "EUser.findById";
    public static final String queryFindByUsername = "EUser.findByUsername";
    public static final String queryFindByUsernameStatus = "EUser.findByUsernameStatus";
    public static final String queryFindByIdUsername = "EUser.findByIdUsername";
    public static final String queryFindByUsernamePasswordStatus = "EUser.findByUsernamePasswordStatus";
    public static final String querySearchByUsername = "EUser.searchByUsername";
    
    
    //*****************************************************
    //*                 Entity variables
    //*****************************************************
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "name")
    @Basic(optional = false)
    private String name;

    @Column(name = "username")
    @Basic(optional = false)
    private String username;
    
    @Column(name = "password")
    @Basic(optional = false)
    private String password;
    
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    private UserStatusEnum status;
    
    @Column(name = "user_type")
    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    private UserTypeEnum user_type;
    
    @Column(name = "address")
    @Basic(optional = true)
    private String address;
    
    @Column(name = "money")
    @Basic(optional = false)
    private Float money;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<EUserSession> sessionsCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<EUserRequest> requestsCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "senderId")
    private Collection<EMessage> messagesSent;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "receiverId")
    private Collection<EMessage> messagesReceived;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<ETransaction> transactionsCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<EBid> bidsCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<EItem> itemsCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reporter")
    private Collection<EReport> reportsCreated;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user_reported")
    private Collection<EReport> reportsBy;
    
    //follows many to many?
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
//    private Collection<EFollow> followsCollection;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "pd_follow", joinColumns = {
        @JoinColumn(name = "userId", referencedColumnName = "id")},
            inverseJoinColumns = {
                @JoinColumn(name = "itemId",
                   referencedColumnName = "id")})
    private Collection<EItem> followedItemsCollection;
    
    //*****************************************************
    //*             CONSTRUCTORS / GETS / SETS
    //*****************************************************
    public EUser() {
    }

    public EUser(Long id) {
        this.id = id;
    }

    public EUser(Long id, String name, String username, String password, UserStatusEnum status, UserTypeEnum user_type, String address, Float money) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.status = status;
        this.user_type = user_type;
        this.address = address;
        this.money = money;
    }

    public EUser(String name, String username, String password, UserStatusEnum status, UserTypeEnum user_type, String address, Float money) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.status = status;
        this.user_type = user_type;
        this.address = address;
        this.money = money;
    }

    public EUser(String name, String username, String password, String address) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.status = UserStatusEnum.TOBEAPROVED;
        this.user_type = UserTypeEnum.NORMAL;
        this.address = address;
        this.money = (float) 100.0;
    } 
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserStatusEnum getStatus() {
        return status;
    }

    public void setStatus(UserStatusEnum status) {
        this.status = status;
    }

    public UserTypeEnum getUser_type() {
        return user_type;
    }

    public void setUser_type(UserTypeEnum user_type) {
        this.user_type = user_type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Float getMoney() {
        return money;
    }

    public void setMoney(Float money) {
        this.money = money;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<EUserRequest> getRequestsCollection() {
        return requestsCollection;
    }

    public void setRequestsCollection(Collection<EUserRequest> requestsCollection) {
        this.requestsCollection = requestsCollection;
    }

    public Collection<EMessage> getMessagesSent() {
        return messagesSent;
    }

    public void setMessagesSent(Collection<EMessage> messagesSent) {
        this.messagesSent = messagesSent;
    }

    public Collection<EMessage> getMessagesReceived() {
        return messagesReceived;
    }

    public void setMessagesReceived(Collection<EMessage> messagesReceived) {
        this.messagesReceived = messagesReceived;
    }

    public Collection<ETransaction> getTransactionsCollection() {
        return transactionsCollection;
    }

    public void setTransactionsCollection(Collection<ETransaction> transactionsCollection) {
        this.transactionsCollection = transactionsCollection;
    }

    public Collection<EBid> getBidsCollection() {
        return bidsCollection;
    }

    public void setBidsCollection(Collection<EBid> bidsCollection) {
        this.bidsCollection = bidsCollection;
    }

    public Collection<EItem> getFollowedItemsCollection() {
        return followedItemsCollection;
    }

    public void setFollowedItemsCollection(Collection<EItem> followedItemsCollection) {
        this.followedItemsCollection = followedItemsCollection;
    }

    public Collection<EUserSession> getSessionsCollection() {
        return sessionsCollection;
    }

    public void setSessionsCollection(Collection<EUserSession> sessionsCollection) {
        this.sessionsCollection = sessionsCollection;
    }

    public Collection<EItem> getItemsCollection() {
        return itemsCollection;
    }

    public void setItemsCollection(Collection<EItem> itemsCollection) {
        this.itemsCollection = itemsCollection;
    }

    public Collection<EReport> getReportsCreated() {
        return reportsCreated;
    }

    public Collection<EReport> getReportsBy() {
        return reportsBy;
    }
    
    

    //*****************************************************
    //*                 Other functions
    //*****************************************************
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EUser)) {
            return false;
        }
        EUser other = (EUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entitys.EUser[ id=" + id + " ]";
    }

    
    
}

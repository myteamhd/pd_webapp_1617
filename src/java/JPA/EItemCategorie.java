/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Dav__
 */
@Entity
@Table(name="pd_item_categorie")
@NamedQueries({
    @NamedQuery(name="EItemCategorie.findAll", query="SELECT c from EItemCategorie c"),
    @NamedQuery(name="EItemCategorie.findById", query="SELECT c from EItemCategorie c WHERE c.id = :id")
})
public class EItemCategorie implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static final String queryFindAll = "EItemCategorie.findAll";
    public static final String queryFindById = "EItemCategorie.findById";
    
    //*****************************************************
    //*                 Entity variables
    //*****************************************************
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "name")
    @Basic(optional = false)
    private String name;
    
    @Column(name = "description")
    @Basic(optional = true)
    private String description;
   
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categorieId")
    private Collection<EItem> itemsCollection;

    //*****************************************************
    //*             CONSTRUCTORS / GETS / SETS
    //*****************************************************
    public EItemCategorie() {
    }

    public EItemCategorie(Long id) {
        this.id = id;
    }

    public EItemCategorie(Long id, String name, String description, Collection<EItem> itemsCollection) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.itemsCollection = itemsCollection;
    }

    public EItemCategorie(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<EItem> getItemsCollection() {
        return itemsCollection;
    }

    public void setItemsCollection(Collection<EItem> itemsCollection) {
        this.itemsCollection = itemsCollection;
    }

    //*****************************************************
    //*                 Other functions
    //*****************************************************
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EItemCategorie)) {
            return false;
        }
        EItemCategorie other = (EItemCategorie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JPA.EItemCategorie[ id=" + id + " ]";
    }
    
}

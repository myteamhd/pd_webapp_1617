/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dav__
 */
@Entity
@Table(name="pd_newsletter")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ENewsletter.findLastItems", query = "SELECT n FROM ENewsletter n ORDER BY n.createdAt DESC, n.createdAt DESC ")
})

public class ENewsletter implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static final String queryFindLastItems = "ENewsletter.findLastItems";
    
    //*****************************************************
    //*                 Entity variables
    //*****************************************************
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "message")
    @Basic(optional = false)
    private String message;
    
    @Column(name = "createdAt")
    @Basic(optional = false)
    private Date createdAt;

    //*****************************************************
    //*             CONSTRUCTORS / GETS / SETS
    //*****************************************************
    public ENewsletter() {
    }

    public ENewsletter(Integer id, String message, Date createdAt) {
        this.id = id;
        this.message = message;
        this.createdAt = createdAt;
    }

    public ENewsletter(String message) {
        this.message = message;
        this.createdAt = Calendar.getInstance().getTime();
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedAt() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/M/dd HH:mm:SS");
        return sdf.format(createdAt);
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    
    //*****************************************************
    //*                 Other functions
    //*****************************************************
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ENewsletter)) {
            return false;
        }
        ENewsletter other = (ENewsletter) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entitys.ENewsletter[ id=" + id + " ]";
    }

}

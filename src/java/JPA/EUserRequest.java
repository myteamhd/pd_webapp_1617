/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Dav__
 */
@Entity
@Table(name="pd_user_request")
@NamedQueries({
    @NamedQuery(name = "EUserRequest.findActiveRequestByIdClosed", query="SELECT r FROM EUserRequest r WHERE r.id = :id AND r.closed = :closed"),
    @NamedQuery(name = "EUserRequest.findAllActive", query="Select r from EUserRequest r WHERE r.closed = :closed")
})
public class EUserRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static final String queryFindActiveRequestByIdClosed = "EUserRequest.findActiveRequestByIdClosed";
    public static final String queryFindAllActive = "EUserRequest.findAllActive";
    
    //*****************************************************
    //*                 Entity variables
    //*****************************************************
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "message")
    @Basic(optional = true)
    private String message;
    
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    private RequestTypeEnum type;
    
    @Column(name = "closed")
    @Basic(optional = false)
    private Boolean closed;
    
    @Column(name = "createdAt")
    @Basic(optional = false)
    private Date createdAt;
    
    @JoinColumn(name = "userId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EUser userId;

    //*****************************************************
    //*             CONSTRUCTORS / GETS / SETS
    //*****************************************************
    public EUserRequest() {
    }

    public EUserRequest(Long id) {
        this.id = id;
    }

    public EUserRequest(Long id, String message, RequestTypeEnum type, Boolean closed, EUser userId) {
        this.id = id;
        this.message = message;
        this.type = type;
        this.closed = closed;
        this.userId = userId;
        this.createdAt = new Date();
    }

    public EUserRequest(String message, RequestTypeEnum type, Boolean closed, EUser userId) {
        this.message = message;
        this.type = type;
        this.closed = closed;
        this.userId = userId;
        this.createdAt = new Date();
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RequestTypeEnum getType() {
        return type;
    }

    public void setType(RequestTypeEnum type) {
        this.type = type;
    }

    public Boolean getClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    public EUser getUserId() {
        return userId;
    }

    public void setUserId(EUser userId) {
        this.userId = userId;
    }

    public String getCreatedAt() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/M/dd HH:mm:SS");
        return sdf.format(createdAt);
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    

    //*****************************************************
    //*                 Other functions
    //*****************************************************
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EUserRequest)) {
            return false;
        }
        EUserRequest other = (EUserRequest) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JPA.EUserRequest[ id=" + id + " ]";
    }
    
}

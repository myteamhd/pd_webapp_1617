/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Dav__
 */
@Entity
@Table(name="pd_bid")
public class EBid implements Serializable {

    private static final long serialVersionUID = 1L;
    
//    *****************************************************
//    *                 Entity variables
//    *****************************************************
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "bid_value")
    @Basic(optional = false)
    private Float bid_value;
    
    @JoinColumn(name = "userId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EUser userId;
    
    @JoinColumn(name = "itemId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EItem itemId;
    
    @Column(name = "createdAt")
    @Basic(optional = false)
    private Date createdAt;
    
//    *****************************************************
//    *             CONSTRUCTORS / GETS / SETS
//    *****************************************************
    public EBid() {
    }

    public EBid(Long id) {
        this.id = id;
    }

    public EBid(Long id, Float bid_value, EUser userId, EItem itemId) {
        this.id = id;
        this.bid_value = bid_value;
        this.userId = userId;
        this.itemId = itemId;
        this.createdAt = new Date();
    }

    public EBid(Float bid_value, EUser userId, EItem itemId) {
        this.bid_value = bid_value;
        this.userId = userId;
        this.itemId = itemId;
        this.createdAt = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getBid_value() {
        return bid_value;
    }

    public void setBid_value(Float bid_value) {
        this.bid_value = bid_value;
    }

    public EUser getUserId() {
        return userId;
    }

    public void setUserId(EUser userId) {
        this.userId = userId;
    }

    public EItem getItemId() {
        return itemId;
    }

    public void setItemId(EItem itemId) {
        this.itemId = itemId;
    }
    
    public String getCreatedAt() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");
        return sdf.format(createdAt);
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    
//    *****************************************************
//    *                 Other functions
//    *****************************************************
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EBid)) {
            return false;
        }
        EBid other = (EBid) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entitys.EBid[ id=" + id + " ]";
    }
    
}

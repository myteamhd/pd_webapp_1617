/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Dav__
 */
@Entity
@Table(name="pd_report")
@NamedQueries({
    @NamedQuery(name = "EReport.findActiveRequestByIdClosed", query="SELECT r FROM EReport r WHERE r.id = :id AND r.closed = :closed"),
    @NamedQuery(name = "EReport.findAllActive", query="Select r from EReport r WHERE r.closed = :closed ORDER BY r.createdAt DESC")
})
public class EReport implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static final String queryFindActiveRequestByIdClosed = "EReport.findActiveRequestByIdClosed";
    public static final String queryFindAllActive = "EReport.findAllActive";
    
    //*****************************************************
    //*                 Entity variables
    //*****************************************************
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "motive")
    @Basic(optional = false)
    private String motive;
    
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    private ReportTypeEnum type;
    
    @Column(name = "closed")
    @Basic(optional = false)
    private Boolean closed;
    
    @Column(name = "createdAt")
    @Basic(optional = false)
    private Date createdAt;
    
    @JoinColumn(name = "reporter", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EUser reporter;

    @JoinColumn(name = "user_reported", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private EUser user_reported;
    
    @JoinColumn(name = "item_reported", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private EItem item_reported;
    
    //*****************************************************
    //*             CONSTRUCTORS / GETS / SETS
    //*****************************************************
    public EReport() {
    }

    public EReport(Long id) {
        this.id = id;
    }

    public EReport(Long id, String motive, ReportTypeEnum type, EUser reporter, EUser user_reported, EItem item_reported) {
        this.id = id;
        this.motive = motive;
        this.type = type;
        this.closed = false;
        this.createdAt = new Date();
        this.reporter = reporter;
        this.user_reported = user_reported;
        this.item_reported = item_reported;
    }

    public EReport(String motive, ReportTypeEnum type, EUser reporter, EUser user_reported) {
        this.motive = motive;
        this.type = type;
        this.closed = false;
        this.createdAt = new Date();
        this.reporter = reporter;
        this.user_reported = user_reported;
    }

    public EReport(String motive, ReportTypeEnum type, EUser reporter, EItem item_reported) {
        this.motive = motive;
        this.type = type;
        this.closed = false;
        this.createdAt = new Date();
        this.reporter = reporter;
        this.item_reported = item_reported;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getCreatedAt() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/M/dd HH:mm:SS");
        return sdf.format(createdAt);
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getMotive() {
        return motive;
    }

    public void setMotive(String motive) {
        this.motive = motive;
    }

    public ReportTypeEnum getType() {
        return type;
    }

    public void setType(ReportTypeEnum type) {
        this.type = type;
    }

    public Boolean getClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    public EUser getReporter() {
        return reporter;
    }

    public void setReporter(EUser reporter) {
        this.reporter = reporter;
    }

    public EUser getUser_reported() {
        return user_reported;
    }

    public void setUser_reported(EUser user_reported) {
        this.user_reported = user_reported;
    }

    public EItem getItem_reported() {
        return item_reported;
    }

    public void setItem_reported(EItem item_reported) {
        this.item_reported = item_reported;
    }
    
    

    //*****************************************************
    //*                 Other functions
    //*****************************************************
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EReport)) {
            return false;
        }
        EReport other = (EReport) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JPA.EUserRequest[ id=" + id + " ]";
    }
    
}

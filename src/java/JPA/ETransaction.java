/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Dav__
 */
@Entity
@Table(name="pd_transaction")
@NamedQueries({
    @NamedQuery(name = "ETransaction.findAll", query = "SELECT t from ETransaction t ORDER BY t.createdAt DESC"),
    @NamedQuery(name = "ETransaction.findAllByUserId", query = "SELECT t from ETransaction t WHERE t.userId = :userId ORDER BY t.createdAt DESC"),
    
})
public class ETransaction implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static final String queryFindAll = "ETransaction.findAll";
    public static final String queryFindAllByUserId = "ETransaction.findAllByUserId";
    
    //*****************************************************
    //*                 Entity variables
    //*****************************************************
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "transaction_value")
    @Basic(optional = false)
    private Float transaction_value;

    @Column(name = "createdAt")
    @Basic(optional = false)
    private Date createdAt;
    
    @JoinColumn(name = "userId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EUser userId;
    
    @JoinColumn(name = "itemId", referencedColumnName = "id")
    @OneToOne(optional = false)
    private EItem itemId;
    
    //*****************************************************
    //*             CONSTRUCTORS / GETS / SETS
    //*****************************************************
    public ETransaction() {
    }

    public ETransaction(Long id) {
        this.id = id;
    }

    public ETransaction(Long id, Float transaction_value, EUser userId, EItem itemId) {
        this.id = id;
        this.transaction_value = transaction_value;
        this.createdAt = new Date();
        this.userId = userId;
        this.itemId = itemId;
    }

    public ETransaction(Float transaction_value, EUser userId, EItem itemId) {
        this.transaction_value = transaction_value;
        this.createdAt = new Date();
        this.userId = userId;
        this.itemId = itemId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getTransaction_value() {
        return transaction_value;
    }

    public void setTransaction_value(Float transaction_value) {
        this.transaction_value = transaction_value;
    }

    public String getCreatedAt() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        return sdf.format(createdAt);
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public EUser getUserId() {
        return userId;
    }

    public void setUserId(EUser userId) {
        this.userId = userId;
    }

    public EItem getItemId() {
        return itemId;
    }

    public void setItemId(EItem itemId) {
        this.itemId = itemId;
    }

    //*****************************************************
    //*                 Other functions
    //*****************************************************
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ETransaction)) {
            return false;
        }
        ETransaction other = (ETransaction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JPA.ETransaction[ id=" + id + " ]";
    }
    
}

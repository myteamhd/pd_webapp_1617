/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * @author Dav__
 */
@Entity
@Table(name="pd_item")
@NamedQueries({
    @NamedQuery(name = "EItem.findAll", query = "SELECT i from EItem i ORDER BY i.createdAt DESC"),
    @NamedQuery(name = "EItem.findById", query = "SELECT i from EItem i WHERE i.id = :id"),
    @NamedQuery(name = "EItem.findAllByUserId", query = "SELECT i from EItem i WHERE i.userId = :userId ORDER BY i.createdAt DESC"),
    @NamedQuery(name = "EItem.findAllByIdByUserId", query = "SELECT i from EItem i WHERE i.id = :id AND i.userId = :userId ORDER BY i.createdAt DESC"),
    @NamedQuery(name = "EItem.findAllInAuction", query = "SELECT i from EItem i WHERE i.status = :status and i.date_max_bids > :date ORDER BY i.createdAt DESC")
})
public class EItem implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static final String queryFindAll = "EItem.findAll";
    public static final String queryFindById = "EItem.findById";
    public static final String queryFindAllByUserId = "EItem.findAllByUserId";
    public static final String queryFindAllByIdByUserId = "EItem.findAllByIdByUserId";
    public static final String queryFindAllInAuction = "EItem.findAllInAuction";
    
//    *****************************************************
//    *                 Entity variables
//    *****************************************************
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @JoinColumn(name = "userId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EUser userId;

    @Column(name = "name")
    @Basic(optional = false)
    private String name;
    
    @Column(name = "description")
    @Basic(optional = true)
    private String description;
    
    @Column(name = "price_initial")
    @Basic(optional = false)
    private Float price_initial;
    
    @Column(name = "price_buy")
    @Basic(optional = false)
    private Float price_buy;
    
    @Column(name = "date_max_bids")
    @Basic(optional = false)
    private Date date_max_bids;
    
    @Column(name = "createdAt")
    @Basic(optional = false)
    private Date createdAt;
    
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    private ItemStatusEnum status;
    
    @JoinColumn(name = "categorieId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EItemCategorie categorieId;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "itemId")
    private ETransaction transaction;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "itemId")
    @OrderBy("id DESC")
    private Collection<EBid> bidsCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "item_reported")
    private Collection<EReport> reportsBy;
    
    //follows many to many?
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "itemId")
//    private Collection<EFollow> followsCollection;
    
    @ManyToMany(mappedBy = "followedItemsCollection",
                       cascade = CascadeType.ALL)
    private Collection<EUser> followUsersCollection;
    
    
//    *****************************************************
//    *             CONSTRUCTORS / GETS / SETS
//    *****************************************************
    public EItem() {
    }

    public EItem(Long id) {
        this.id = id;
    }

    public EItem(Long id, String name, String description, Float price_initial, Float price_buy, Date date_max_bids, ItemStatusEnum status, EItemCategorie categorieId, ETransaction transaction, Collection<EBid> bidsCollection, Collection<EUser> followUsersCollection) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price_initial = price_initial;
        this.price_buy = price_buy;
        this.date_max_bids = date_max_bids;
        this.status = status;
        this.categorieId = categorieId;
        this.transaction = transaction;
        this.bidsCollection = bidsCollection;
        this.followUsersCollection = followUsersCollection;
        this.createdAt = new Date();
    }

    public EItem(String name, String description, Float price_initial, Float price_buy, Date date_max_bids, EItemCategorie categorieId, EUser owner) {
        this.name = name;
        this.description = description;
        this.price_initial = price_initial;
        this.price_buy = price_buy;
        this.date_max_bids = date_max_bids;
        this.status = ItemStatusEnum.ONBIDDING;
        this.categorieId = categorieId;
        this.createdAt = new Date();
        this.userId = owner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice_initial() {
        return price_initial;
    }

    public void setPrice_initial(Float price_initial) {
        this.price_initial = price_initial;
    }

    public Float getPrice_buy() {
        return price_buy;
    }

    public void setPrice_buy(Float price_buy) {
        this.price_buy = price_buy;
    }

    public String getDate_max_bids() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        return sdf.format(date_max_bids);
    }

    public void setDate_max_bids(Date date_max_bids) {
        this.date_max_bids = date_max_bids;
    }

    public ItemStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ItemStatusEnum status) {
        this.status = status;
    }

    public EItemCategorie getCategorieId() {
        return categorieId;
    }

    public void setCategorieId(EItemCategorie categorieId) {
        this.categorieId = categorieId;
    }

    public ETransaction getTransaction() {
        return transaction;
    }

    public void setTransaction(ETransaction transaction) {
        this.transaction = transaction;
    }

    public Collection<EBid> getBidsCollection() {
        return bidsCollection;
    }
    
    public Float getLastBidValue(){
        ArrayList<EBid> bids = new ArrayList<>(getBidsCollection());
        if(bids.size() > 0) {
            return bids.get(0).getBid_value();
        } 
            
        return this.price_initial;    
    }

    public void setBidsCollection(Collection<EBid> bidsCollection) {
        this.bidsCollection = bidsCollection;
    }

    public Collection<EUser> getFollowUsersCollection() {
        return followUsersCollection;
    }

    public void setFollowUsersCollection(Collection<EUser> followUsersCollection) {
        this.followUsersCollection = followUsersCollection;
    }

    public String getCreatedAt() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");
        return sdf.format(createdAt);
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public EUser getOwner() {
        return userId;
    }

    public void setOwner(EUser owner) {
        this.userId = owner;
    }

    public EUser getUserId() {
        return userId;
    }

    public void setUserId(EUser userId) {
        this.userId = userId;
    }

    public Collection<EReport> getReportsBy() {
        return reportsBy;
    }

    public void setReportsBy(Collection<EReport> reportsBy) {
        this.reportsBy = reportsBy;
    }
    
    
    
//    *****************************************************
//    *                 Other functions
//    *****************************************************
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EItem)) {
            return false;
        }
        EItem other = (EItem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entitys.EItems[ id=" + id + " ]";
    }
    
}

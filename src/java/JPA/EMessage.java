/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Dav__
 */
@Entity
@Table(name="pd_message")
@NamedQueries({
    @NamedQuery(name="EMessage.findAllByUserReceiver", query="Select m from EMessage m where m.receiverId = :receiverId ORDER BY m.createdAt DESC"),
    @NamedQuery(name="EMEssage.findAllByUserSender", query="Select m from EMessage m where m.senderId = :senderId ORDER BY m.createdAt DESC"),
    @NamedQuery(name="EMessage.findById", query="Select m from EMessage m where m.id = :id")
})
public class EMessage implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static String queryFindAllByUserReceiver = "EMessage.findAllByUserReceiver";
    public static String queryFindAllByUserSender = "EMEssage.findAllByUserSender";
    public static String queryFindAllById = "EMessage.findById";
    
//    *****************************************************
//    *                 Entity variables
//    *****************************************************
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "message")
    @Basic(optional = false)
    private String message;

    @JoinColumn(name = "senderId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EUser senderId;
    
    @JoinColumn(name = "receiverId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EUser receiverId;
    
    @Basic(optional = false)
    @Column(name = "createdAt")
    private Date createdAt;
    
//    *****************************************************
//    *             CONSTRUCTORS / GETS / SETS
//    *****************************************************
    public EMessage() {
    }

    public EMessage(Long id) {
        this.id = id;
    }

    public EMessage(Long id, String message, EUser senderId, EUser receiverId) {
        this.id = id;
        this.message = message;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.createdAt = new Date();
    }

    public EMessage(String message, EUser senderId, EUser receiverId) {
        this.message = message;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.createdAt = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public EUser getSenderId() {
        return senderId;
    }

    public void setSenderId(EUser senderId) {
        this.senderId = senderId;
    }

    public EUser getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(EUser receiverId) {
        this.receiverId = receiverId;
    }

    public String getCreatedAt() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/M/dd HH:mm:SS");
        return sdf.format(createdAt);
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    

//    *****************************************************
//    *                 Other functions
//    *****************************************************
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EMessage)) {
            return false;
        }
        EMessage other = (EMessage) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entitys.EMessage[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManageBeans;

import EJB.Exceptions.Error400Exception;
import EJB.reports.ReportsManagerLocal;
import JPA.EItem;
import JPA.EReport;
import JPA.EUser;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Dav__
 */
@Named(value = "reportsController")
@SessionScoped
public class ReportsController implements Serializable {

    @EJB
    ReportsManagerLocal reportsManager;
    
    @Inject
    UserController userController;
    
    private Integer offset = 0;
    private Integer numResults = 0;
    
    private Long id;
    private String motive;
    
//    private EReport report;
    
    public ReportsController() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        
        try{
            id = Long.parseLong(params.get("id"));
        } catch (Exception e){
            id = null;
        }

        String o = params.get("offset");
        
        if(o != null){
            offset = Integer.parseInt(o);
        }else{
            offset = 0;
        }
        
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getNumResults() {
        return numResults;
    }

    public void setNumResults(Integer numResults) {
        this.numResults = numResults;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMotive() {
        return motive;
    }

    public void setMotive(String motive) {
        this.motive = motive;
    }
    
//    public Long getReportId(){
//        return this.report.getId();
//    }
//    
//    public String getReportMotive(){
//        return this.report.getMotive();
//    }
//    
//    public String getReportCreatedDate(){
//        return this.report.getCreatedAt();
//    }   
//    
//    public EUser getReportUserCreator(){
//        return this.report.getReporter();
//    }
//    
//    public EUser getReportUserReported(){
//        return this.report.getUser_reported();
//    }
//    
//    public EItem getReportItemReported(){
//        return this.report.getItem_reported();
//    }
    
    public String validateParams(){
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        
        try{
            id = Long.parseLong(params.get("id"));
        } catch (Exception e){
            id = null;
        }
        
        if(id == null){
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Falta de parametros!", ""));
            return "/user/auctions";
        }
        
        return null;
    }
    
    public String reportItem(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        try {
            if(reportsManager.reportItem(motive, userController.getLoggedUser().getId(), id)){
                motive = null;
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Denuncia efetuada com sucesso!", ""));
                return "/user/auctions";
            }
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            Logger.getLogger(ReportsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public String reportUser(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        try {
            if(reportsManager.reportUser(motive, userController.getLoggedUser().getId(), id)){
                motive = null;
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Denuncia efetuada com sucesso!", ""));
                return "/user/auctions";
            }
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            Logger.getLogger(ReportsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public String closeReport(Long reportId){
        FacesContext context = FacesContext.getCurrentInstance();

        if(reportsManager.closeReport(reportId)){
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Denuncia fechada!", ""));
            return "/admin/reports";
        }
 
        return null;
    }
    
    public List<EReport> getAllReports(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        if(offset == null || offset < 0){
            offset = 0;
        }
        
        try {
            List<EReport> reports = reportsManager.getReports(offset);
            numResults = reports.size();
            return reports;
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            Logger.getLogger(ReportsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManageBeans;

import EJB.Exceptions.Error400Exception;
import EJB.auctions.AuctionsManagerLocal;
import EJB.auctions.ItemCategoriesManagerLocal;
import EJB.auctions.ItemsManagerLocal;
import JPA.EBid;
import JPA.EItem;
import JPA.EItemCategorie;
import JPA.ItemStatusEnum;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.NormalScope;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.resource.spi.EISSystemException;

/**
 *
 * @author Dav__
 */
@Named(value = "itemsController")
@SessionScoped
public class ItemsController implements Serializable{

    private Integer offset = 0;
    private Integer numResults = 0;
    
    private EItem item;
    
    private String name;
    private String description;
    private Float initial_price;
    private Float buy_price;
    private Date limit_date;
    private ItemStatusEnum status;
    private Long categorieId;
    
    private Float bid_value;
    
    private static Map<ItemStatusEnum,ItemStatusEnum> itemStatusValues;
    static {
        itemStatusValues = new LinkedHashMap<>();
        itemStatusValues.put(ItemStatusEnum.ONBIDDING, ItemStatusEnum.ONBIDDING);
        itemStatusValues.put(ItemStatusEnum.SOLD, ItemStatusEnum.SOLD);
        itemStatusValues.put(ItemStatusEnum.TIMEOVER, ItemStatusEnum.TIMEOVER);
        itemStatusValues.put(ItemStatusEnum.CANCELED, ItemStatusEnum.CANCELED);
    }
    
    @Inject
    UserController userController;
    
    @EJB
    ItemsManagerLocal itemsManager;
    
    @EJB
    ItemCategoriesManagerLocal itemCategoriesManager;
    
    @EJB
    AuctionsManagerLocal auctionsManager;
    
    public ItemsController() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        String o = params.get("offset");
        
        if(o != null){
            offset = Integer.parseInt(o);
        }else{
            offset = 0;
        }
        
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getNumResults() {
        return numResults;
    }

    public void setNumResults(Integer numResults) {
        this.numResults = numResults;
    }
    
    public Long getItemId(){
        return item.getId();
    }

    public String getItemName() {
        return item.getName();
    }

    public void setItemName(String name) {
        item.setName(name);
    }

    public String getItemDescription() {
        return item.getDescription();
    }

    public void setItemDescription(String description) {
        item.setDescription(description);
    }

    public Float getItemInitial_price() {
        return item.getPrice_initial();
    }

    public void setItemInitial_price(Float initial_price) {
        item.setPrice_initial(initial_price);
    }

    public Float getItemBuy_price() {
        return item.getPrice_buy();
    }

    public void setItemBuy_price(Float buy_price) {
        item.setPrice_buy(buy_price);
    }

    public String getItemLimit_date() {
        return item.getDate_max_bids();
    }
    
    public String getItemOwnerName(){
        return item.getOwner().getName();
    }
    
    public Long getItemOwnerId(){
        return item.getOwner().getId();
    }

    public void setItemLimit_date(String limit_date) {
        FacesContext context = FacesContext.getCurrentInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date d = null;
        try {
            d = sdf.parse(limit_date);
        } catch (ParseException ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida!", ""));
        }
        item.setDate_max_bids(d);
    }

    public ItemStatusEnum getItemStatus() {
        return item.getStatus();
    }

    public void setItemStatus(ItemStatusEnum status) {
        item.setStatus(status);
    }

    public Long getItemCategorie() {
        return item.getCategorieId().getId();
    }
    
    public String getItemCategorieName() {
        return item.getCategorieId().getName();
    }
    
    public List<EBid> getItemBidsCollection() {
        return (List<EBid>) item.getBidsCollection();
    }

    public void setItemCategorie(Long categorieId) {
        FacesContext context = FacesContext.getCurrentInstance();
        EItemCategorie categorie = null;
        try {
            categorie = itemCategoriesManager.findCategorieById(categorieId);
        } catch (Error400Exception ex) {
            Logger.getLogger(ItemsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        item.setCategorieId(categorie);
    }

    public Map<ItemStatusEnum, ItemStatusEnum> getItemStatusValues() {
        return itemStatusValues;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getInitial_price() {
        return initial_price;
    }

    public void setInitial_price(Float initial_price) {
        this.initial_price = initial_price;
    }

    public Float getBuy_price() {
        return buy_price;
    }

    public void setBuy_price(Float buy_price) {
        this.buy_price = buy_price;
    }

    public String getLimit_date() {
        if(limit_date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            return sdf.format(limit_date);
        }
        
        return null;
    }

    public void setLimit_date(String limit_date) {
        FacesContext context = FacesContext.getCurrentInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date d = null;
        try {
            d = sdf.parse(limit_date);
        } catch (ParseException ex) {
            //Logger.getLogger(ItemsController.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida!", ""));
        }
        this.limit_date = d;
    }

    public ItemStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ItemStatusEnum status) {
        this.status = status;
    }

    public Long getCategorieId() {
        return categorieId;
    }

    public void setCategorieId(Long categorieId) {
        this.categorieId = categorieId;
    }

    public Float getBid_value() {
        return bid_value;
    }

    public void setBid_value(Float bid_value) {
        this.bid_value = bid_value;
    }

    
    public void validateDate(FacesContext fc, UIComponent uic, Object valor) throws ValidatorException{
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date d = null;
        try {
            d = sdf.parse(valor.toString());
        } catch (ParseException ex) {
            FacesMessage fmsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida!!", ""); 
            throw new ValidatorException(fmsg);
        }
        
    }
    
    public void validateBidValue(FacesContext fc, UIComponent uic, Object valor) throws ValidatorException{
        Float value = (Float) valor;
        
        List<EBid> bids = getItemBidsCollection();
        
        if(bids != null && bids.size() > 0 ){
            if(bids.get(0).getBid_value() >= value){
                FacesMessage fmsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "A bid tem que ser superior ao último valor!!", ""); 
                throw new ValidatorException(fmsg);
            }
        }else{
            if(item.getPrice_initial() >= value){
                FacesMessage fmsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "A bid tem que ser superior ao valor inicial!!", ""); 
                throw new ValidatorException(fmsg);
            }
        }
    }
    
    public String findItem(){
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        String id = params.get("id");
        
        if(id!=null){
            try {
                item = itemsManager.findItembyId(Long.parseLong(id));
            } catch (Error400Exception ex) {
                //Logger.getLogger(ItemCategoriesController.class.getName()).log(Level.SEVERE, null, ex);
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        }
        
        return null;
    }
    
    public String findItemOwnedByUser(){
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        String id = params.get("id");
        
        if(id!=null){
            try {
                item = itemsManager.findItemByIdByUser(Long.parseLong(id), userController.getLoggedUser().getId());
                
                if(item == null){
                    context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item não encontrado ou não tem permissões para ver esse item!", ""));
                    return "/user/items";
                }
                
            } catch (Error400Exception ex) {
                //Logger.getLogger(ItemCategoriesController.class.getName()).log(Level.SEVERE, null, ex);
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        }else {
            return "/user/items";
        }
        
        return null;
    }
    
    public String saveItem(){
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            itemsManager.updateItem(item);
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Item atualizado com sucesso!", ""));
        } catch (Error400Exception ex) {
            //Logger.getLogger(ItemCategoriesController.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        return null;
    }
    
    public Map<String, Long> getAllCategories(){
        
        LinkedHashMap<String,Long> categories = new LinkedHashMap<>();
        
        for(EItemCategorie cat : itemCategoriesManager.getAllItemCategoriesWithoutOffset()){
            categories.put(cat.getName(), cat.getId());
        }
        
        return categories;
    }
    
    public List<EItem> getAllItems(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        if(offset == null || offset < 0){
            offset = 0;
        }
        
        try {
            List<EItem> items = itemsManager.getAllItems(offset);
            this.numResults = items.size();
            return items;
        } catch (Error400Exception ex) {
            //Logger.getLogger(ItemCategoriesController.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return null;
    }
    
    public List<EItem> getAllItemsByUser(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        if(offset == null || offset < 0){
            offset = 0;
        }
        
        try {
            List<EItem> items = itemsManager.getAllItemsByUser(userController.getLoggedUser().getId(), offset);
            this.numResults = items.size();
            return items;
        } catch (Error400Exception ex) {
            //Logger.getLogger(ItemCategoriesController.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return null;
    }
    
    public List<EItem> getAllItemsInAuction(){
        
        FacesContext context = FacesContext.getCurrentInstance();
        
        if(offset == null || offset < 0){
            offset = 0;
        }
        
        try {
            List<EItem> items = itemsManager.getAllItemsInAuction(offset);
            this.numResults = items.size();
            return items;
        } catch (Error400Exception ex) {
            //Logger.getLogger(ItemCategoriesController.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return null;
        
    }
    
    public String cancelOwnItem(){
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if(itemsManager.cancelOwnItem(item.getId())){
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Item cancelado com sucesso!", ""));
            }
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return "/user/items"; 
    }
    
    public String cancelItem(){
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if(itemsManager.cancelItem(item.getId())){
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Item cancelado com sucesso!", ""));
            }
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return "/admin/itemlist";
    }
    
    public String reactivateItem(){
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if(itemsManager.reactivateItem(item.getId())){
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Item reactivado com sucesso!", ""));
            }
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return "/admin/itemlist";
    }
    
    public String createItem(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        try {
            if(itemsManager.createItem(name, description, initial_price, buy_price, limit_date, categorieId, userController.getLoggedUser().getId())){
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Item criado com sucesso!" , ""));
                return "/user/items";
            }
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            Logger.getLogger(RequestsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public String bidItem(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        try {
            if(auctionsManager.createBid(userController.getLoggedUser().getId(), item.getId(), bid_value)){
                bid_value = null;
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Bid efetuada com sucesso!" , ""));
                return findItem();
            } else {
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bid não efetuada!" , ""));
            } 
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            Logger.getLogger(ItemsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public String buyItem(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        try {
            if(auctionsManager.createBid(userController.getLoggedUser().getId(), item.getId(), item.getPrice_buy())){
                bid_value = null;
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Compra efetuada com sucesso!" , ""));
                return findItem();
            } else {
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Compra não efetuada!" , ""));
            } 
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            Logger.getLogger(ItemsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
}

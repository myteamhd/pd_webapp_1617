/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManageBeans;

import EJB.Exceptions.Error400Exception;
import EJB.Exceptions.Error401Exception;
import EJB.Exceptions.Error403Exception;
import EJB.account.AccountManagerLocal;
import JPA.UserTypeEnum;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import JPA.EUser;
import JPA.UserStatusEnum;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Dav__
 */
@Named(value = "userController")
@SessionScoped
public class UserController implements Serializable {

//    @EJB
//    EJBUserLocal user;
    
    @EJB
    AccountManagerLocal accountManager;
    
    private EUser loggedUser;
    
    
    //----- form fields -----
    private String username;
    private String old_password;
    private String password;
    private String conf_password;
    
    
    public UserController() {
    }

    public EUser getLoggedUser(){
        return loggedUser;
    }
    
    public UserTypeEnum getLoggedUserType() {
        if(loggedUser == null){
            return null;
        }
        return loggedUser.getUser_type();
    }

    public String getLoggedName() {
        if(loggedUser == null){
            return null;
        }
        return loggedUser.getName();
    }
    
    public void setLoggedName(String name){
        loggedUser.setName(name);
    }

    public String getLoggedUsername() {
        if(loggedUser == null){
            return null;
        }
        return loggedUser.getUsername();
    }
    
    public void setLoggedUsername(String username){
        loggedUser.setUsername(username);
    }
      
    public String getLoggedAddress(){
        return loggedUser.getAddress();
    }
    
    public void setLoggedAddress(String address){
        loggedUser.setAddress(address);
    }
    
    public UserStatusEnum getLoggedUserStatus(){
        return loggedUser.getStatus();
    }
    
    public Float getLoggedMoney(){
        return loggedUser.getMoney();
    }
    
    public void setLoggedMoney(Float money){
        loggedUser.setMoney(money);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConf_password() {
        return conf_password;
    }

    public void setConf_password(String conf_password) {
        this.conf_password = conf_password;
    }

    public String getOld_password() {
        return old_password;
    }

    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }
    
    @PostConstruct
    public void postConstruct(){
        this.username = "david";
        this.password = "mercurio26";
        login();
    }
    
    @PreDestroy
    public void preDestroy(){
        logout();
    }
    
    public String login(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        try {
            loggedUser = accountManager.login(username, password);
            username = null;
            password = null;
            return "index";
        } catch (Error403Exception ex) {
            //Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        } catch (Error400Exception ex) {
            //Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        
        return null;
    }
    
    public String logout(){
        try {
            if(accountManager.logout(loggedUser)){
                loggedUser = null;
                username = null;
                password = null;
                return "/login";
            }
        } catch (Error400Exception ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public String validateUserPermissions(){
        if(loggedUser != null){
            return null;
        }else{
            return "/login";
        }
    }
    
    public String validateAdminPermissions(){
        if(loggedUser != null && loggedUser.getUser_type() == UserTypeEnum.ADMIN){
            return null;
        }else{
            return "/index"; //redirecionar para página de permissões
        }
    }
    
    public void validaPassword(FacesContext fc, UIComponent uic, Object valor) throws ValidatorException{
        
        if(valor.toString().length() < 6){
           FacesMessage fmsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Password demasiado pequena! Mínimo 6 caracteres!", ""); 
           throw new ValidatorException(fmsg);
        }
        
    }
    
    public String saveUser(){
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if(accountManager.updateUser(loggedUser) != null){
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Alterações guardadas!", ""));
            }
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return null;
    }
    
    public String changePassword(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        try {
            if(accountManager.changePassword(loggedUser.getId(), old_password, password, conf_password)){
                 context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Password alterada com sucesso!", ""));
            }
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        } catch (Error401Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        return null;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManageBeans;

import EJB.Exceptions.Error400Exception;
import EJB.Messages.MessagesManagerLocal;
import EJB.account.AccountManagerLocal;
import JPA.EMessage;
import JPA.EUser;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Dav__
 */
@Named(value = "messageController")
@RequestScoped
public class MessageController implements Serializable{
    
    private Integer offset = 0;
    private Integer numResults = 0;
    
    private String message;
    private String receiver;
    
    private List<EUser> searchResults;

    @Inject
    UserController userController;
    
    @EJB
    MessagesManagerLocal messagesManager;
    
    @EJB
    AccountManagerLocal accountManager;
    
    public MessageController() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        String o = params.get("offset");
        
        if(o != null){
            offset = Integer.parseInt(o);
        }else{
            offset = 0;
        }
        
        receiver = params.get("username");
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getNumResults() {
        return numResults;
    }

    public void setNumResults(Integer numResults) {
        this.numResults = numResults;
    }

    public List<EUser> getSearchResults() {
        return searchResults;
    }

    public void setSearchResults(List<EUser> searchResults) {
        this.searchResults = searchResults;
    }
    
    public List<EMessage> getAllSentMessages(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        if(userController.getLoggedUser() != null){
            try {
                List<EMessage> messages = messagesManager.getAllSentMessages(userController.getLoggedUser().getId(), offset);
                numResults = messages.size();
                return messages;
            } catch (Error400Exception ex) {
               context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        }
        
        return null;
    }
    
    public List<EMessage> getAllReceivedMessages(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        if(userController.getLoggedUser() != null){
            try {
                List<EMessage> messages = messagesManager.getAllReceivedMessages(userController.getLoggedUser().getId(), offset);
                numResults = messages.size();
                return messages;
            } catch (Error400Exception ex) {
               context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        }
        
        return null;
    }
    
    public String sendNewMessage(){
        FacesContext context = FacesContext.getCurrentInstance();

        try {
            if(messagesManager.sendMessage(message, userController.getLoggedUser().getId(), receiver)){
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensagem enviada!", ""));
                receiver = null;
                return "/user/messagesent";
            }
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return null;
    }

    public void searchUser(){
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if(receiver.length() > 1){
                searchResults = accountManager.searchUser(receiver);
                numResults = searchResults.size();
            }else{
                searchResults = new ArrayList<EUser>();
                numResults = searchResults.size();
            }
        } catch (Error400Exception ex) {
            searchResults = new ArrayList<EUser>();
            numResults = searchResults.size();
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        } 
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManageBeans;

import EJB.Exceptions.Error400Exception;
import EJB.Exceptions.Error404Exception;
import EJB.auctions.ItemCategoriesManagerLocal;
import JPA.EItemCategorie;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Dav__
 */
@Named(value = "itemCategoriesController")
@SessionScoped
public class ItemCategoriesController implements Serializable{

    private Integer offset = 0;
    private Integer numResults = 0;
    
    private EItemCategorie categorie;
    
    private String name;
    private String description;
    
    @EJB
    ItemCategoriesManagerLocal itemCategoriesManager;
    
    public ItemCategoriesController() {
        
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        String o = params.get("offset");
        
        if(o != null){
            offset = Integer.parseInt(o);
        }else{
            offset = 0;
        }
        
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getNumResults() {
        return numResults;
    }

    public void setNumResults(Integer numResults) {
        this.numResults = numResults;
    }
    
    public String getCategorieName(){
        return categorie.getName();
    }
    
    public void setCategorieName(String name){
        categorie.setName(name);
    }
    
    public String getCategorieDescription(){
        return categorie.getDescription();
    }
    
    public void setCategorieDescription(String description){
        categorie.setDescription(description);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public List<EItemCategorie> getAllItemCategories(){
        return itemCategoriesManager.getAllItemCategories(offset);
    }
    
    public String findItemCategorie(){
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        String id = params.get("id");
        
        if(id!=null){
            try {
                categorie = itemCategoriesManager.findCategorieById(Long.parseLong(id));
            } catch (Error400Exception ex) {
                //Logger.getLogger(ItemCategoriesController.class.getName()).log(Level.SEVERE, null, ex);
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        }
        
        return null;
    }
    
    public String saveItemCategorie(){
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            itemCategoriesManager.updateItemCategorie(categorie);
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Categoria guardada com sucesso!", ""));
        } catch (Error400Exception ex) {
            //Logger.getLogger(ItemCategoriesController.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        return null;
    }
    
    public String removeItemCategorie(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        try {
            itemCategoriesManager.removeItemCategorie(categorie.getId());
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Categoria removida com sucesso!", ""));
        } catch (Error404Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return "/admin/itemcategorieslist";
    }
    
    public String createNewItemCategorie(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        try {
            itemCategoriesManager.createItemCategorie(name, description);
             context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Categoria criada com sucesso!", ""));
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return "/admin/itemcategorieslist";
    }
}

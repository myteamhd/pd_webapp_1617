/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManageBeans;

import EJB.newsletter.NewsletterManagerLocal;
import JPA.ENewsletter;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Dav__
 */
@Named(value = "newsletterController")
@RequestScoped
public class NewsletterController {
    
    private Integer offset = 0;
    private Integer numResults = 0;
    
    @EJB
    NewsletterManagerLocal newsletterManager;

    public NewsletterController() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        String o = params.get("offset");
        
        if(o != null){
            offset = Integer.parseInt(o);
        }else{
            offset = 0;
        }
        
    }
    
    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
    
    public Integer getNumResults() {
        return numResults;
    }

    public void setNumResults(Integer numResults) {
        this.numResults = numResults;
    }
    
    public List<ENewsletter> getLastNewsletterItems(){
        if(offset == null || offset < 0){
            offset = 0;
        }
        
        List<ENewsletter> news = newsletterManager.getLastNewsletterItems(offset);
        this.numResults = news.size();
        return news;
    }
}

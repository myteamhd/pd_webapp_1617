/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManageBeans;

import EJB.Exceptions.Error400Exception;
import EJB.account.AccountManagerLocal;
import JPA.EUser;
import JPA.UserStatusEnum;
import JPA.UserTypeEnum;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Dav__
 */
@Named(value = "accountsController")
@SessionScoped
public class AccountsController implements Serializable{
    
    private Integer offset = 0;
    private Integer numResults = 0;
    
    private Long userId;
    private EUser user;
    
    private static Map<UserStatusEnum,UserStatusEnum> userStatusValues;
    static {
        userStatusValues = new LinkedHashMap<>();
        userStatusValues.put(UserStatusEnum.ACTIVE, UserStatusEnum.ACTIVE);
        userStatusValues.put(UserStatusEnum.BLOCKED, UserStatusEnum.BLOCKED);
        userStatusValues.put(UserStatusEnum.INACTIVE, UserStatusEnum.INACTIVE);
        userStatusValues.put(UserStatusEnum.TOBEAPROVED, UserStatusEnum.TOBEAPROVED);
    }
    
    private static Map<UserTypeEnum,UserTypeEnum> userTypeValues;
    static {
        userTypeValues = new LinkedHashMap<>();
        userTypeValues.put(UserTypeEnum.ADMIN, UserTypeEnum.ADMIN);
        userTypeValues.put(UserTypeEnum.NORMAL, UserTypeEnum.NORMAL);
    }
    
    @Inject
    UserController userController;
    
    @Inject
    NotificationsController notificationsController;
    
    @EJB
    AccountManagerLocal accountManager;
    
    public AccountsController() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        String o = params.get("offset");
        
        if(o != null){
            offset = Integer.parseInt(o);
        }else{
            offset = 0;
        }
        
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getNumResults() {
        return numResults;
    }

    public void setNumResults(Integer numResults) {
        this.numResults = numResults;
    }

    public EUser getUser() {
        return user;
    }

    public void setUser(EUser user) {
        this.user = user;
    }
    
    public String getName(){
        return user.getName();
    }
    
    public void setName(String name){
        user.setName(name);
    }
    
    public String getUsername(){
        return user.getUsername();
    }
    
    public void setUsername(String username){
        user.setUsername(username);
    }
    
    public String getAddress(){
        return user.getAddress();
    }
    
    public void setAddress(String address){
        user.setAddress(address);
    }
    
    public UserTypeEnum getUserType(){
        return user.getUser_type();
    }
    
    public void setUserType(UserTypeEnum userType){
        user.setUser_type(userType);
    }
    
    public UserStatusEnum getUserStatus(){
        return user.getStatus();
    }
    
    public void setUserStatus(UserStatusEnum status){
        user.setStatus(status);
    }
    
    public Float getMoney(){
        return user.getMoney();
    }
    
    public void setMoney(Float money){
        user.setMoney(money);
    }

    public Map<UserStatusEnum, UserStatusEnum> getUserStatusValues() {
        return userStatusValues;
    }

    public Map<UserTypeEnum, UserTypeEnum> getUserTypeValues() {
        return userTypeValues;
    }
    
    public List<EUser> getAllUsers(){
        
        if(offset == null || offset < 0){
            offset = 0;
        }
        
        if(userController.getLoggedUserType() == UserTypeEnum.ADMIN){
            List<EUser> results = accountManager.getAllUsers(offset);
            numResults = results.size();
            return results;
        }
        
        return null;
    }
    
    public String findUserAccount(){
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        String id = params.get("id");
        
        if(id != null){
            userId = Long.parseLong(id);
            user = accountManager.getUserById(userId);
            return null;
        }
        
        return "/index";
    }
    
    public String saveUser(){
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if(accountManager.updateUser(user) != null){
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Alterações guardadas!", ""));
            }
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return null;
    }
    
    public String blockAccount(){
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if(accountManager.blockAccount(user.getId())){
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Utilziador bloqueado com sucesso!", ""));
            }
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return "/admin/accounts";
    }
    
    public String reactivateAccount(){
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if(accountManager.reactivateAccount(user.getId())){
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Utilziador reativado com sucesso!", ""));
            }
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return "/admin/accounts";
    }
    
    public String reportUser(){
        
        return null;
    }
    
}

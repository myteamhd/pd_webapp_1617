/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManageBeans;

import EJB.Exceptions.Error400Exception;
import EJB.auctions.TransactionManagerLocal;
import JPA.EItem;
import JPA.ETransaction;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Dav__
 */
@Named(value = "transactionsController")
@RequestScoped
public class TransactionsController {

    @EJB
    TransactionManagerLocal transactionManager;
    
    @Inject
    UserController userController;
    
    private Integer offset = 0;
    private Integer numResults = 0;
    
    public TransactionsController() {
        
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        String o = params.get("offset");
        
        if(o != null){
            offset = Integer.parseInt(o);
        }else{
            offset = 0;
        }
        
    }
    
    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getNumResults() {
        return numResults;
    }

    public void setNumResults(Integer numResults) {
        this.numResults = numResults;
    }
    
    public List<ETransaction> getAllItemsByUser(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        if(offset == null || offset < 0){
            offset = 0;
        }

        List<ETransaction> transactions = transactionManager.getAllTransactionsByUser(userController.getLoggedUser().getId(), offset);
        this.numResults = transactions.size();
        return transactions;

    }
    
    public List<ETransaction> getLastItems(){
        FacesContext context = FacesContext.getCurrentInstance();

        List<ETransaction> transactions = transactionManager.getLastTransactions();
        this.numResults = transactions.size();
        
        return transactions;

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManageBeans;

import EJB.Exceptions.Error400Exception;
import EJB.account.UserRequestManagerLocal;
import JPA.EUserRequest;
import JPA.UserTypeEnum;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Dav__
 */
@Named(value = "notificationsController")
@RequestScoped
public class NotificationsController implements Serializable{

    @Inject
    UserController userController;
    
    @EJB
    UserRequestManagerLocal userRequestManager;
    
    private Integer offset = 0;
    private Integer numResults = 0;
    
    public NotificationsController() {
        FacesContext context;
        context =  FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        String o = params.get("offset");
        if(o != null){
            offset = Integer.parseInt(o);
        }else{
            offset = 0;
        }
        
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getNumResults() {
        return numResults;
    }

    public void setNumResults(Integer numResults) {
        this.numResults = numResults;
    }
    
    
    public List<EUserRequest> getAllUserRequests(){
        
        if(offset == null || offset < 0){
            offset = 0;
        }
        
        if(userController.getLoggedUserType() == UserTypeEnum.ADMIN){
            List<EUserRequest> results = userRequestManager.getAllActive(offset);
            numResults = results.size();
            return results;
        }
        
        return null;
    }
    
    public String acceptRequest(Long itemId){
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if(userRequestManager.acceptRequest(itemId)){
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pedido aceite!", ""));
            }
        } catch (Error400Exception ex) {
            //Logger.getLogger(NotificationsController.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return null;
    }
    
    public String rejectRequest(Long itemId){
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if(userRequestManager.rejectRequest(itemId)){
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pedido rejeitado!", ""));
            }
        } catch (Error400Exception ex) {
            //Logger.getLogger(NotificationsController.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        
        return null;
    }
    
}

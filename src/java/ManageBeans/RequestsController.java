/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManageBeans;

import EJB.Exceptions.Error400Exception;
import EJB.Exceptions.Error500Exception;
import EJB.account.AccountManagerLocal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Dav__
 */
@Named(value = "requestsController")
@RequestScoped
public class RequestsController {
    
    @EJB
    AccountManagerLocal accountManager;

    private String username;
    private String password;
    private String conf_password;
    private String name;
    private String address;
    
    public RequestsController() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConf_password() {
        return conf_password;
    }

    public void setConf_password(String conf_password) {
        this.conf_password = conf_password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    public String requestAccess(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        if(password.equals(conf_password)){
            try {
                if(accountManager.createUserByRequest(name, username, password, address)){
                    context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pedido efetuado com sucesso!" , ""));
                    return "/index";
                }
            } catch (Error400Exception ex) {
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
                Logger.getLogger(RequestsController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Error500Exception ex) {
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
                Logger.getLogger(RequestsController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Passwords não coincidem!", ""));
        }
        
        return null;
    }
    
    public String reactivateAcount(){
        FacesContext context = FacesContext.getCurrentInstance();
        
        try {
            if(accountManager.createReativationRequest(username)){
                context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pedido de ativação efetuado com sucesso!" , ""));
                return "/index";
            }
        } catch (Error400Exception ex) {
            context.addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            Logger.getLogger(RequestsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public void validaPassword(FacesContext fc, UIComponent uic, Object valor) throws ValidatorException{
        
        if(valor.toString().length() < 6){
           FacesMessage fmsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Password demasiado pequena! Mínimo 6 caracteres!", ""); 
           throw new ValidatorException(fmsg);
        }
        
    }
    
}

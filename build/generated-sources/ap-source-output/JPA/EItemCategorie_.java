package JPA;

import JPA.EItem;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-10T21:07:31")
@StaticMetamodel(EItemCategorie.class)
public class EItemCategorie_ { 

    public static volatile SingularAttribute<EItemCategorie, String> name;
    public static volatile CollectionAttribute<EItemCategorie, EItem> itemsCollection;
    public static volatile SingularAttribute<EItemCategorie, String> description;
    public static volatile SingularAttribute<EItemCategorie, Long> id;

}
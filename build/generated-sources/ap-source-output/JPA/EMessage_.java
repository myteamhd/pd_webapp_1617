package JPA;

import JPA.EUser;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-10T21:07:31")
@StaticMetamodel(EMessage.class)
public class EMessage_ { 

    public static volatile SingularAttribute<EMessage, Date> createdAt;
    public static volatile SingularAttribute<EMessage, EUser> senderId;
    public static volatile SingularAttribute<EMessage, EUser> receiverId;
    public static volatile SingularAttribute<EMessage, Long> id;
    public static volatile SingularAttribute<EMessage, String> message;

}
package JPA;

import JPA.EBid;
import JPA.EItemCategorie;
import JPA.ETransaction;
import JPA.EUser;
import JPA.ItemStatusEnum;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-10T21:07:31")
@StaticMetamodel(EItem.class)
public class EItem_ { 

    public static volatile SingularAttribute<EItem, Float> price_buy;
    public static volatile SingularAttribute<EItem, String> description;
    public static volatile SingularAttribute<EItem, Date> date_max_bids;
    public static volatile SingularAttribute<EItem, EUser> userId;
    public static volatile SingularAttribute<EItem, Date> createdAt;
    public static volatile SingularAttribute<EItem, EItemCategorie> categorieId;
    public static volatile SingularAttribute<EItem, String> name;
    public static volatile CollectionAttribute<EItem, EBid> bidsCollection;
    public static volatile CollectionAttribute<EItem, EUser> followUsersCollection;
    public static volatile SingularAttribute<EItem, Long> id;
    public static volatile SingularAttribute<EItem, Float> price_initial;
    public static volatile SingularAttribute<EItem, ETransaction> transaction;
    public static volatile SingularAttribute<EItem, ItemStatusEnum> status;

}
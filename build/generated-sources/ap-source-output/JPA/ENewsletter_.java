package JPA;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-10T21:07:31")
@StaticMetamodel(ENewsletter.class)
public class ENewsletter_ { 

    public static volatile SingularAttribute<ENewsletter, Date> createdAt;
    public static volatile SingularAttribute<ENewsletter, Integer> id;
    public static volatile SingularAttribute<ENewsletter, String> message;

}
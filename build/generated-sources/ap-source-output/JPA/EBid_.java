package JPA;

import JPA.EItem;
import JPA.EUser;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-10T21:07:31")
@StaticMetamodel(EBid.class)
public class EBid_ { 

    public static volatile SingularAttribute<EBid, EItem> itemId;
    public static volatile SingularAttribute<EBid, Date> createdAt;
    public static volatile SingularAttribute<EBid, Float> bid_value;
    public static volatile SingularAttribute<EBid, Long> id;
    public static volatile SingularAttribute<EBid, EUser> userId;

}
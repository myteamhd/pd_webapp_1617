package JPA;

import JPA.EUser;
import JPA.RequestTypeEnum;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-10T21:07:31")
@StaticMetamodel(EUserRequest.class)
public class EUserRequest_ { 

    public static volatile SingularAttribute<EUserRequest, Date> createdAt;
    public static volatile SingularAttribute<EUserRequest, Boolean> closed;
    public static volatile SingularAttribute<EUserRequest, Long> id;
    public static volatile SingularAttribute<EUserRequest, String> message;
    public static volatile SingularAttribute<EUserRequest, RequestTypeEnum> type;
    public static volatile SingularAttribute<EUserRequest, EUser> userId;

}
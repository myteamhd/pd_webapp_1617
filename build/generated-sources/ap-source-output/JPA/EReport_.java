package JPA;

import JPA.EItem;
import JPA.EUser;
import JPA.ReportTypeEnum;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-10T21:07:31")
@StaticMetamodel(EReport.class)
public class EReport_ { 

    public static volatile SingularAttribute<EReport, Date> createdAt;
    public static volatile SingularAttribute<EReport, EItem> item_reported;
    public static volatile SingularAttribute<EReport, String> motive;
    public static volatile SingularAttribute<EReport, EUser> user_reported;
    public static volatile SingularAttribute<EReport, Boolean> closed;
    public static volatile SingularAttribute<EReport, EUser> reporter;
    public static volatile SingularAttribute<EReport, Long> id;
    public static volatile SingularAttribute<EReport, ReportTypeEnum> type;

}
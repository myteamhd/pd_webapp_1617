package JPA;

import JPA.EBid;
import JPA.EItem;
import JPA.EMessage;
import JPA.ETransaction;
import JPA.EUserRequest;
import JPA.EUserSession;
import JPA.UserStatusEnum;
import JPA.UserTypeEnum;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-10T21:07:31")
@StaticMetamodel(EUser.class)
public class EUser_ { 

    public static volatile SingularAttribute<EUser, String> address;
    public static volatile CollectionAttribute<EUser, EMessage> messagesSent;
    public static volatile CollectionAttribute<EUser, EUserRequest> requestsCollection;
    public static volatile CollectionAttribute<EUser, EItem> itemsCollection;
    public static volatile CollectionAttribute<EUser, EItem> followedItemsCollection;
    public static volatile CollectionAttribute<EUser, ETransaction> transactionsCollection;
    public static volatile SingularAttribute<EUser, String> password;
    public static volatile SingularAttribute<EUser, UserTypeEnum> user_type;
    public static volatile SingularAttribute<EUser, Float> money;
    public static volatile CollectionAttribute<EUser, EMessage> messagesReceived;
    public static volatile SingularAttribute<EUser, String> name;
    public static volatile CollectionAttribute<EUser, EBid> bidsCollection;
    public static volatile SingularAttribute<EUser, Long> id;
    public static volatile SingularAttribute<EUser, String> username;
    public static volatile SingularAttribute<EUser, UserStatusEnum> status;
    public static volatile CollectionAttribute<EUser, EUserSession> sessionsCollection;

}